import numpy as np


def get_input_capacitor_width(resolution, resonator_length: float) -> tuple or None:
    """
    Returns the input capacitor width depending on the resonator length of
    the filter or the filter resolution.

    :param resolution: resolution of the filter (100, 200, or 300)
    :param resonator_length: Length of the resonator corresponding to this capacitor (m).
    :return:
    """

    if resolution != 100 and resolution != 200 and resolution != 300:
        print("Error filter resolution can only be 100, 200 or 300")
        return None

    if resolution == 100:
        m = 0.103
        c = -5.77

    if resolution == 200:
        m = 0.0683
        c = -3.55

    if resolution == 300:
        m = 0.0648
        c = -5.39

    return (m * (resonator_length * 1e6) + c) * 1e-6


def get_output_capacitor_width(resolution, resonator_length: float) -> tuple or None:
    """
    Returns the output capacitor width depending on the resonator length of the filter or the filter resolution.

    :param resolution: resolution of the filter (100, 200, or 300)
    :param resonator_length: Length of the resonator corresponding to this capacitor (m).
    :return:
    """

    if resolution != 100 and resolution != 200 and resolution != 300:
        print("Error filter resolution can only be 100, 200 or 300")
        return None

    if resolution == 100:
        m = 0.0812
        c = -5.14

    if resolution == 200:
        m = 0.0595
        c = -4.76

    if resolution == 300:
        m = 0.0390
        c = -1.98

    return (m * (resonator_length * 1e6) + c) * 1e-6


def get_resonator_length(resolution, target_f0: float) -> tuple or None:
    """
    Returns the resonator length in microns depending on the target f0 of the filter and the filter resolution.

    :param target_f0: Target f0 of the filter (Hz).
    :param resolution: resolution of the filter (100, 200, or 300)
    :return:
    """

    if resolution != 100 and resolution != 200 and resolution != 300:
        print("Error filter resolution can only be 100, 200 or 300")
        return None

    if resolution == 100:
        m = -1.0465
        c = 13.673

    if resolution == 200:
        m = -1.0412
        c = 13.670

    if resolution == 300:
        m = -1.0372
        c = 13.665

    return (10 ** -(c / m)) * ((target_f0) ** (1 / m)) * 1e-6
