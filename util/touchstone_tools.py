import numpy as np


def get_port_parameters(string) -> dict:
    """
    Extracts port variables from comments in touchstone data.

    :param string:
    :return: Dictionary of port variables.
    """
    pos_in_line = 0
    port_dictionary = {}

    while pos_in_line < len(string):

        # Determine index of next equals sign.
        equals_pos = string.find("=", pos_in_line, len(string))
        # If no more equals signs, break loop.
        if equals_pos == -1:
            break

        # Read back from equals sign to extract varName
        var_name_start_pos = string.rfind(" ", pos_in_line, equals_pos)
        var_name = string[var_name_start_pos:equals_pos].strip()

        # If statement to extract value corresponding to varName
        # If single values

        if string[equals_pos + 1] != "(":
            var_end_pos = string.find(" ", equals_pos, len(string))
            var = string[equals_pos + 1 : var_end_pos].strip()
        # If complex, i.e in a bracket.
        else:
            var_end_pos = string.find(") ", equals_pos, len(string))
            split_pos = string.find("+", equals_pos, len(string))
            var_real = float(string[equals_pos + 2 : split_pos].strip())  # First value
            var_imag = float(
                string[split_pos + 3 : var_end_pos].strip(" )")
            )  # Second value
            var = complex(var_real, var_imag)

        # Add varName and var as key name and value to port dictionary.
        port_dictionary[var_name] = var
        # Update posInLine to end of var.
        pos_in_line = var_end_pos

    return port_dictionary


def get_mean_port_impedance(file_path, port_number):
    """
    Function to return the mean impedance of a given port across a frequency range from a touchstone file.
    Impedance returned for unpacking as meanR, meanX, Rerr, Xerr

    :param file_path: Absolute path to file containing data.
    :param port_number: A string of an integer representing the desired port.
    e.g. for port 1 use "1".
    :return: The mean impedance and error of a given port.
    """

    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        impedance_data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                impedance_data_points += 1

    with open(file_path, "r") as reader:
        port_impedances = {
            "F": np.empty(shape=impedance_data_points),
            "Z0": np.empty(shape=impedance_data_points, dtype=complex),
        }
        data_point = 0
        # If line starts with '!< PN' where N is the portNumber, extract the dictionary of parameters then
        # collect frequency and corresponding Z0 values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_impedances["F"][data_point] = current_port_params["F"]
                port_impedances["Z0"][data_point] = current_port_params["Z0"]

                data_point += 1

        mean_r = np.mean(port_impedances["Z0"].real)
        r_err = (
            abs(np.max(port_impedances["Z0"].real) - np.min(port_impedances["Z0"].real))
            / 2
        )
        mean_x = np.mean(port_impedances["Z0"].imag)
        x_err = (
            abs(np.max(port_impedances["Z0"].imag) - np.min(port_impedances["Z0"].imag))
            / 2
        )

    return mean_r, mean_x, r_err, x_err


def get_port_z0_array(file_path, port_number: str) -> np.ndarray:
    """
    Returns an array of port Z0 values extracted from an output
    touchstone file of a sonnet simulation. These Z0 values correspond to the
    port impedance at a certain frequency. The corresponding frequencies can be
    extracted using the getPortFArray() method.

    :param file_path: Absolute path to file containing data.
    :param port_number: A string of an integer representing the desired port.
    e.g. for port 1 use "1".
    :return: Numpy array of port Z0 values.
    """

    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_impedances = np.empty(shape=(data_points), dtype=complex)
        data_point = 0
        # If line starts with '!< PN' where N is the portNumber, extract the
        # dictionary of parameters then collect frequency and corresponding Z0
        # values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_impedances[data_point] = current_port_params["Z0"]

                data_point += 1

    return port_impedances


def get_port_f_array(file_path, port_number: str) -> np.ndarray:
    """
    Returns an array of frequency values at which the port z0 and
    effective_permittivity were evaluated in the sonnet touchstone file.

    :param file_path: Absolute path to file containing data.
    :param port_number: A string of an integer representing the desired port.
    e.g. for port 1 use "1".
    :return: Numpy array of frequency values.
    """

    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_frequencies = np.empty(shape=data_points, dtype=float)
        data_point = 0
        # If line starts with '!< PN' where N is the portNumber, extract the
        # dictionary of parameters then collect frequency and corresponding Z0
        # values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_frequencies[data_point] = current_port_params["F"]

                data_point += 1

    return port_frequencies


def get_port_eeff_array(file_path, port_number: str) -> np.ndarray:
    """
    Returns an array of port effective_permittivity values extracted
    from an output touchstone file of a sonnet simulation. These
    effective_permittivity values correspond to the
    port impedance at a certain frequency. The corresponding frequencies can be
    extracted using the getPortFArray() method.

    :param file_path: Absolute path to file containing data.
    :param port_number: A string of an integer representing the desired port.
     e.g. for port 1 use "1".
    :return: Array of port effective permittivity values as a numpy array.
    """

    with open(file_path, "r") as reader:
        # Counter used to find the number of impedance data points in the file:
        data_points = 0
        for line in reader:
            if "! P" + port_number in line:
                data_points += 1

    with open(file_path, "r") as reader:
        port_permittivities = np.empty(shape=(data_points), dtype=complex)
        data_point = 0
        # If line starts with '!< PN' where N is the portNumber, extract the
        # dictionary of parameters then collect frequency and corresponding Z0
        # values across the data file into portImpedance.
        for line in reader:
            if "! P" + port_number in line:
                current_port_params = get_port_parameters(line)
                port_permittivities[data_point] = current_port_params["Eeff"]

                data_point += 1

    return port_permittivities


def get_mean_port_permittivity(file_path, port_number):
    """
    Function to return the mean impedance of a given port across a frequency range from a touchstone file.
    Impedance returned for unpacking as meanR, meanX, Rerr, Xerr

    :param file_path: Absolute path to file containing data.
    :param port_number: A string of an integer representing the desired port.
    e.g. for port 1 use "1".
    :return: The mean impedance and error of a given port.
    """

    port_permittivity_array = get_port_eeff_array(
        file_path=file_path, port_number=port_number
    )

    mean_real_permittivity = float(np.mean(np.real(port_permittivity_array)))
    mean_imag_permittivity = float(np.mean(np.imag(port_permittivity_array)))

    return complex(mean_real_permittivity, mean_imag_permittivity)
