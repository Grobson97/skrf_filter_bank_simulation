import skrf as rf
import numpy as np
import util.filter_tools as filter_tools

from scipy import interpolate
from util.models.coupling_capacitor import CouplingCapacitor


class SeriesFilter:
    def __init__(
        self,
        channel_number: int,
        input_media: rf.media.Media,
        resonator_media: rf.media.Media,
        output_media: rf.media.Media,
        target_f0: float,
        resolution: int,
        dielectric_thickness: float,
        etch_error=0.0,
    ) -> None:
        """
        Creates a new instance of a series filter channel modelling a filter from the mm-wave feedline to the filter
        output.

        :param channel_number: Index for the channel.
        :param input_media: rf.Media object to use to create the small input transmission line.
        :param output_media: rf.Media object to use to create the small output transmission line.
        :param target_f0: Target f0 of the filter channel (Hz).
        :param dielectric_thickness: Thickness of the dielectric (in meters).
        :param resolution: Filter resolution (Only 100, 200 or 300 are allowed).
        :param etch_error: Adds an error to the dimensions of the capacitor widths. Nominally set to 0.
        """

        self.channel_number = channel_number
        self.input_media = input_media
        self.resonator_media = resonator_media
        self.output_media = output_media
        self.target_f0 = target_f0
        self.resolution = resolution
        self.dielectric_thickness = dielectric_thickness
        self.etch_error = etch_error

        self.resonator_length = filter_tools.get_resonator_length(
            resolution=resolution, target_f0=target_f0
        )
        self.input_capacitor_width = filter_tools.get_input_capacitor_width(
            resolution=resolution, resonator_length=self.resonator_length
        )
        self.output_capacitor_width = filter_tools.get_output_capacitor_width(
            resolution=resolution, resonator_length=self.resonator_length
        )

        self.input_capacitor = CouplingCapacitor(
            input_media=input_media,
            output_media=resonator_media,
            capacitor_width=self.input_capacitor_width + etch_error,
            dielectric_thickness=dielectric_thickness,
            input_capacitor=True,
        )
        self.output_capacitor = CouplingCapacitor(
            input_media=resonator_media,
            output_media=output_media,
            capacitor_width=self.output_capacitor_width + etch_error,
            dielectric_thickness=dielectric_thickness,
            input_capacitor=False,
        )

    def create_network(self) -> rf.Network:
        """
        Function to create a network object for the coupling capacitor instance.

        :return:
        """

        # Create component network objects:
        input_line = self.input_media.line(d=4.3, unit="um")
        input_capacitor_network = self.input_capacitor.create_network()
        resonator_line = self.resonator_media.line(
            d=self.resonator_length * 1e6 - 8, unit="um"
        )  # - 8 to account for the length in the capacitor networks.
        output_capacitor_network = self.output_capacitor.create_network()
        output_line = self.output_media.line(d=13.1, unit="um")

        series_filter_network = (
            input_line
            ** input_capacitor_network
            ** resonator_line
            ** output_capacitor_network
            ** output_line
        )

        return series_filter_network
