import skrf as rf
from scipy import interpolate
import numpy as np


class CouplingCapacitor:
    def __init__(
        self,
        input_media: rf.media.Media,
        output_media: rf.media.Media,
        capacitor_width: float,
        dielectric_thickness: float,
        input_capacitor: bool,
    ) -> None:
        """
        Creates a new instance of a coupling capacitor.

        :param input_media: rf.Media object to use to create the small input transmission line.
        :param output_media: rf.Media object to use to create the small output transmission line.
        :param capacitor_width: Width of capacitor (in meters).
        :param dielectric_thickness: Thickness of the dielectric (in meters).
        :param input_capacitor: Boolean to state whether this is an input coupling capacitor or output.
        This changes which interpolation surface is used to obtain capacitance and parasitic L.
        """

        self.input_media = input_media
        self.output_media = output_media
        self.capacitor_width = capacitor_width
        self.dielectric_thickness = dielectric_thickness
        self.input_capacitor = input_capacitor

        # NOTE: If running test scripts, add ../ to the start of the data file strings below:
        if input_capacitor:
            try:
                data_file = np.load(
                    "model_data/input_coupling_capacitor_data_arrays.npz"
                )
            except:
                data_file = np.load(
                    "../model_data/input_coupling_capacitor_data_arrays.npz"
                )
        else:
            try:
                data_file = np.load(
                    "model_data/output_coupling_capacitor_data_arrays.npz"
                )
            except:
                data_file = np.load(
                    "../model_data/output_coupling_capacitor_data_arrays.npz"
                )

        cap_width_array = data_file["cap_width_array"]
        dielectric_thickness_array = data_file["dielectric_thickness_array"]
        capacitance_array = data_file["capacitance_array"]
        parasitic_inductance_array = data_file["parasitic_inductance_array"]

        self.capacitance = interpolate.griddata(
            (cap_width_array, dielectric_thickness_array),
            capacitance_array,
            (capacitor_width, dielectric_thickness),
        )

        self.parasitic_inductance = interpolate.griddata(
            (cap_width_array, dielectric_thickness_array),
            parasitic_inductance_array,
            (capacitor_width, dielectric_thickness),
        )

    def create_network(self) -> rf.Network:
        """
        Function to create a network object for the coupling capacitor instance.
        :return:
        """
        input_line = self.input_media.line(d=4.0, unit="um")
        output_line = self.output_media.line(d=4.0, unit="um")

        # Define Circuit components
        capacitor = self.input_media.capacitor(C=self.capacitance, name="C")
        parasitic_inductor = self.input_media.inductor(
            L=self.parasitic_inductance, name="Parasitic L"
        )

        coupling_capacitor_network = (
            input_line**parasitic_inductor**capacitor**parasitic_inductor**output_line
        )

        return coupling_capacitor_network
