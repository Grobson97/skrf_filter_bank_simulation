import skrf as rf
import numpy as np
import util.media_tools as media_tools

from util.models.series_filter import SeriesFilter


class FilterBank:
    def __init__(
        self,
        frequency_band: rf.Frequency,
        target_f0_array: np.ndarray,
        resolution: int,
        etch_error_array: np.ndarray,
        nominal_dielectric_thickness: float,
        dielectric_thickness_array: np.ndarray,
        loss_tangent: float,
        output_channel_number: int,
        filter_spacing: float,
        oversampling: float,
    ) -> None:
        """Creates a two port rf.Network object representing a filter bank. The output port is placed on the desired
        output channel. All other channels are terminated in a matched load.

        :param frequency_band: instance of the frequency band defining each frequency point that will be evaluated
        :param target_f0_array: array of target F0 values used to define the geometry of each filter (Hz).
        :param resolution: Resolution of the filterbank. Can be one of 100, 200 or 300.
        :param etch_error_array: Array of values that can be added to geometry dimensions due to over or under etching,
        e.g increase line widths. Must be same length as target_f0 array. Each value is the etch error that will be used
        for that corresponding channel
        :param nominal_dielectric_thickness: The nominal dielectric thickness that would represent the expected
        thickness.
        :param dielectric_thickness_array: array of values for each channels dielectric thickness. Must be same length
        as target_f0 array
        :param loss_tangent: Dielectric loss tangent
        :param output_channel_number: The port number to be used as the output. All other channels will be terminated in a
        matched load. If 0, all channels are terminated in a matched load and output port is at the other end of the
        feedline.
        :param filter_spacing: The physical distance between neighbouring filters ("interconnection length") relative to
        the wavelength. e.g. 0.25 is quarter wave spaced. 2.0 would be double wavelength spaced, etc.
        :param oversampling: Oversampling of the filter-bank.
        """

        self.frequency_band = frequency_band
        self.target_f0_array = target_f0_array
        self.resolution = resolution
        self.etch_error_array = etch_error_array
        self.nominal_dielectric_thickness = nominal_dielectric_thickness
        self.dielectric_thickness_array = dielectric_thickness_array
        self.loss_tangent = loss_tangent
        self.output_channel_number = output_channel_number
        self.filter_spacing = filter_spacing
        self.oversampling = oversampling

    def create_network(self) -> rf.Network:
        """
        Function to create a two port rf.Network object representing a filter bank. The output port is placed on the
        desired output channel. All other channels are terminated in a matched load.

        :return:
        """

        feedline_media = media_tools.create_media(
            frequency_band=self.frequency_band,
            microstrip_width=2.5e-6,
            loss_tangent=self.loss_tangent,
            dielectric_thickness=self.nominal_dielectric_thickness,
        )

        # Create parasitic capacitance to ground network object at first channel connection
        parasitic_cap = feedline_media.capacitor(C=-7.951817997309143e-15)
        # Add a short after parasitic capacitor
        parasitic_cap_to_gnd = parasitic_cap ** feedline_media.short()
        parasitic_cap_to_gnd = feedline_media.shunt(
            parasitic_cap_to_gnd, name="Parasitic C1"
        )

        feedline_in = feedline_media.line(110e-6, "m", name="FeedIn")
        feedline_in = feedline_in**parasitic_cap_to_gnd
        feedline_out = feedline_media.line(110e-6, "m", name="FeedOut")

        filter_bank_ntwk_1 = feedline_in

        for index, target_f0 in enumerate(self.target_f0_array):
            channel_number = index + 1

            # select etch Error and dielectric thickness for the new channel.
            etch_error = self.etch_error_array[index]
            dielectric_thickness = self.dielectric_thickness_array[index]

            # Create channel media with fab errors:
            microstrip_media_2um = media_tools.create_media(
                frequency_band=self.frequency_band,
                microstrip_width=2.0e-6 + etch_error,
                dielectric_thickness=dielectric_thickness,
                loss_tangent=self.loss_tangent,
            )
            microstrip_media_3um = media_tools.create_media(
                frequency_band=self.frequency_band,
                microstrip_width=3.0e-6 + etch_error,
                dielectric_thickness=dielectric_thickness,
                loss_tangent=self.loss_tangent,
            )

            series_filter = SeriesFilter(
                channel_number=channel_number,
                input_media=microstrip_media_2um,
                resonator_media=microstrip_media_2um,
                output_media=microstrip_media_3um,
                target_f0=target_f0,
                resolution=self.resolution,
                dielectric_thickness=dielectric_thickness,
            )

            series_filter_network = series_filter.create_network()

            # If not the desired output channel, terminate channel in matched load. (Creates 1 port network)
            if channel_number != self.output_channel_number:
                series_filter_network = (
                    series_filter_network ** microstrip_media_3um.match()
                )

            # Convert channel network into a shunted network. increases number of ports by 1:
            shunt_channel_ntwk = feedline_media.shunt(
                series_filter_network, name="CH" + str(index + 1)
            )

            # renumber ports for the 3 port channel network has the following indices:
            # 0: feedline input
            # 1: feedline output
            # 2: channel output
            if shunt_channel_ntwk.nports == 3:
                shunt_channel_ntwk.renumber([0, 1, 2], [0, 2, 1])

            # Create parasitic capacitance to ground network object at each filter.
            parasitic_cap = feedline_media.capacitor(C=1.295896913918483e-14)

            # Add a short after parasitic capacitor
            parasitic_cap_to_gnd = parasitic_cap ** feedline_media.short()
            parasitic_cap_to_gnd = feedline_media.shunt(
                parasitic_cap_to_gnd, name="Parasitic C-" + str(index + 2)
            )

            # Create microstrip network object to place between filters:
            interconnection_ntwk = feedline_media.line(
                d=2
                * self.filter_spacing
                * series_filter.resonator_length,  # times 2 since res_length is half a wavelength
                unit="m",
                name="Interconnect-" + str(index + 1),
            )
            interconnection_ntwk = interconnection_ntwk**parasitic_cap_to_gnd

            # Filter-bank is built up to the point of the output channel, then the second half is built. Following
            # conditions allow for this:

            # If desired output is not the full band output:
            if self.output_channel_number != 0:
                if channel_number < self.output_channel_number:
                    filter_bank_ntwk_1 = (
                        filter_bank_ntwk_1**shunt_channel_ntwk**interconnection_ntwk
                    )
                if channel_number == self.output_channel_number:
                    output_channel_ntwk = shunt_channel_ntwk
                    filter_bank_ntwk_2 = interconnection_ntwk
                if channel_number > self.output_channel_number:
                    filter_bank_ntwk_2 = (
                        filter_bank_ntwk_2**shunt_channel_ntwk**interconnection_ntwk
                    )

            # If desired output is the full band output:
            if self.output_channel_number == 0:
                filter_bank_ntwk_1 = (
                    filter_bank_ntwk_1**shunt_channel_ntwk**interconnection_ntwk
                )

        # If desired output isn't the full band output then connect both halves of filter bank.
        if self.output_channel_number != 0:
            filter_bank_ntwk = rf.connect(filter_bank_ntwk_1, 1, output_channel_ntwk, 0)
            filter_bank_ntwk = rf.connect(
                filter_bank_ntwk,
                1,
                (filter_bank_ntwk_2**feedline_out ** feedline_media.match()),
                0,
            )
            return filter_bank_ntwk

        # If desired output is the full band output then there is only one filter-bank network:
        if self.output_channel_number == 0:
            return filter_bank_ntwk_1**feedline_out
