import numpy as np
import skrf as rf
import click
import time

from util.models.filter_bank import FilterBank


def get_filter_bank_s_params(
    frequency_band: rf.Frequency,
    target_f0_array: np.ndarray,
    resolution: int,
    etch_error_array: np.ndarray,
    nominal_dielectric_thickness: float,
    dielectric_thickness_array: np.ndarray,
    loss_tangent: float,
    filter_spacing: float,
    oversampling: float,
) -> np.ndarray:
    """Creates a numpy array of all diagonal components of a filter-bank S parameter matrix. e.g. S11, S21....SN1.
    The indices of the s_params matrix represent the following:
    0: S11
    1: S21
    N: SN1, where N-2 is the filter channel number. e.g. index N=3 gives the throughput from port one to filter
    channel 1.
    Method uses a loop to create a new filter-bank each time with a different output port and extracts the
    throughput transmission S matrix, SN1.

    :param frequency_band: instance of the frequency band defining each frequency point that will be evaluated
    :param target_f0_array: array of target F0 values used to define the geometry of each filter.
    :param resolution: Resolution of the filterbank. Can either be 200 or 300.
    :param etch_error_array: Array of values that can be added to geometry dimensions due to over or under etching,
    e.g increase line widths. Must be same length as target_f0 array. Each value is the etch error that will be used
    for that corresponding channel.
    :param nominal_dielectric_thickness: The nominal dielectric thickness that would represent the expected
    thickness.
    :param dielectric_thickness_array: array of values for each channels dielectric thickness. Must be same length
    as target_f0 array
    :param loss_tangent: Dielectric loss tangent
    :param filter_spacing: The physical distance between neighbouring filters ("interconnection length") relative to
    the wavelength. e.g. 0.25 is quarter wave spaced. 2.0 would be double wavelength spaced, etc.
    :param oversampling: Oversampling of the filter-bank.
    """
    click.echo("Getting new filter bank S parameters...")

    s_params = []
    total = target_f0_array.size + 1
    for channel_number in range(total):
        time_channel = time.time()
        filter_bank = FilterBank(
            frequency_band=frequency_band,
            target_f0_array=target_f0_array,
            resolution=resolution,
            etch_error_array=etch_error_array,
            nominal_dielectric_thickness=nominal_dielectric_thickness,
            dielectric_thickness_array=dielectric_thickness_array,
            loss_tangent=loss_tangent,
            output_channel_number=channel_number,
            filter_spacing=filter_spacing,
            oversampling=oversampling,
        )
        filter_bank_network = filter_bank.create_network()
        s_array = filter_bank_network.s
        if channel_number == 0:
            s_params.append(s_array[:, 0, 0])  # S11 of full filter-bank.
        s_params.append(s_array[:, 0, 1])  # SN1 of full filter-bank.

        click.echo(
            f"Got S params for channel {channel_number} of {total-1} ({(time.time() - time_channel):.2f}s)"
        )

    return np.array(s_params)


def create_target_f0_array(
    f_min: float, f_max: float, number_of_channels: int
) -> np.ndarray:
    """Returns a numpy array of log spaced target f0 values between the bounds of the frequency band.

    :param f_min: minimum frequency of the band
    :param f_max: maximum frequency of the band
    :param number_of_channels: number of channels in the filter, should be determined from resolving power and
    oversampling factor.
    :returns: A numpy array of log spaced target f0 values between the bounds of the frequency band.

    """
    target_f0_array = []
    fr = f_max
    while len(target_f0_array) < number_of_channels:
        target_f0_array.append(fr)
        fr = fr * np.exp(-(np.log(f_max) - np.log(f_min)) / (number_of_channels - 1))

    return np.array(target_f0_array)
