import matplotlib.pyplot as plt
import numpy as np
from lmfit import Model
from lmfit import Parameters
from scipy import stats


def filter_power_spectrum(
    frequency_array: np.ndarray,
    f0: float,
    qi: float,
    q_filter: float,
) -> np.ndarray:
    """
    Model of a skewed lorentzian using a complex qc.

    :param frequency_array: Array of N frequency points.
    :param f0: Resonant frequency
    :param qi: Loss quality factor
    :param q_filter: Total Q of the filter.
    :return:
    """

    x = (frequency_array - f0) / f0
    root_term = np.sqrt((2 * (qi**2) * (q_filter**2)) / (qi - q_filter) ** 2)

    return np.abs(q_filter / (root_term * (1 + 2j * q_filter * x))) ** 2


def fit_filter_s31_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    q_filter_guess: float,
    qi_guess: float,
    f0_guess: float or None,
    normalise=True,
    plot_graph=True,
    plot_db=True,
    plot_title="",
) -> dict:
    """
    Function to fit the S31 power spectrum lorentzian model to data..\n
    Returns dictionary of fit parameters with standard errors, e.g. result_dict["qi"] = [value, error].
    Result dictionary keys: dict_keys(['f0', 'qi', 'q_filter', 'peak_height']).

    :param frequency_array: Array of N frequency points.
    :param data_array: S31 Power Magnitude array of N S31 data points.
    :param q_filter_guess: Guess for total filter quality factor.
    :param qi_guess: Guess for filter loss quality factor.
    :param f0_guess: Guess for the resonant frequency.
    :param normalise: Boolean to normalise the data_array array before fitting.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S Parameter in plot_db.
    :param plot_title: Title to give to plot. Default is an empty string.
    :return:
    """

    if normalise:
        data_array = data_array / np.max(data_array)

    f0_index = np.argmax(data_array)  # index of maximum point.
    resonance_data_points = round(
        len(data_array) / 2
    )  # define number of data points either side of resonance to sample
    start_index = f0_index - round(resonance_data_points)
    end_index = f0_index + round(resonance_data_points)
    # if start_index < 0:
    #     start_index = 0
    # if end_index > frequency_array.size - 1:
    #     end_index = -1

    start_index = 0
    end_index = -1

    # Define model:
    l_model = Model(filter_power_spectrum)

    # Allow for user input f0_guess
    f0_first_guess = frequency_array[f0_index]
    if f0_guess is not None:
        f0_first_guess = f0_guess

    # Define Parameters:
    params = Parameters()
    params.add("f0", value=f0_first_guess, vary=True)
    params.add("qi", value=qi_guess, vary=True, min=0)
    params.add("q_filter", value=q_filter_guess, vary=True, min=0)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(data_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    # Extract lorentzian peak height from fit:
    result_dict["peak_height"] = filter_power_spectrum(
        frequency_array=result_dict["f0"][0],
        f0=result_dict["f0"][0],
        qi=result_dict["qi"][0],
        q_filter=result_dict["q_filter"][0],
    )

    if plot_graph:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=1000000,
        )

        power_fit = filter_power_spectrum(
            frequency_array=f_array,
            f0=result_dict["f0"][0],
            qi=result_dict["qi"][0],
            q_filter=result_dict["q_filter"][0],
        )

        if not plot_db:
            y_label = "S21 Magnitude"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            data_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            power_fit,
            linestyle="-",
            color="r",
            label=f"Best fit:\nf0={result_dict['f0'][0]:.2e}\nqr={result_dict['q_filter'][0]:.2e}"
            f"\nQi={result_dict['qi'][0]:.2e}",
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("|S31|$^2$")
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return result_dict


def skewed_lorentzian(
    frequency_array: np.ndarray,
    f0: float,
    qr: float,
    qc_real: float,
    qc_imag: float,
    a: float,
) -> np.ndarray:
    """
    Model of a skewed lorentzian using a complex qc.

    :param frequency_array: Array of N frequency points.
    :param f0: Resonant frequency
    :param qr: Total resonator quality factor
    :param qc_real: Real coupling quality factor
    :param qc_imag: Imaginary coupling quality factor
    :param a: Amplitude
    :return:
    """

    x = (frequency_array - f0) / f0
    return abs(a * (1 - qr / (qc_real + 1j * qc_imag) / (1 + 2j * qr * x)))


def fit_skewed_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    qr_guess: float,
    f0_guess: float or None,
    plot_graph=True,
    plot_dB=True,
    plot_title="",
) -> dict:
    """
    Function to fit a very simple lorentzian bw, f0, I, a to a resonance in S21.\n
    Returns dictionary of fit parameters with standard errors, e.g. result_dict["qr"] = [value, error].

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex array of N S21 data points.
    :param qr_guess: Guess for the qr of the lorentzian.
    :param f0_guess: Guess for the resonant frequency.
    :param plot_graph: Boolean command to plot a graph of the fit.
    :param plot_dB: Boolean command to plot S Parameter in dB.
    :param plot_title: Title to give to plot. Default is an empty string.

    """

    magnitude_array = np.abs(data_array)

    f0_index = np.argmin(magnitude_array)  # index of minimum point.
    resonance_data_points = round(
        len(magnitude_array) / 2
    )  # define number of data points either side of resonance to sample
    start_index = f0_index - round(resonance_data_points)
    end_index = f0_index + round(resonance_data_points)
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size - 1:
        end_index = -1

    # Define model:
    l_model = Model(skewed_lorentzian)

    f0_first_guess = frequency_array[f0_index]
    if f0_guess is not None:
        f0_first_guess = f0_guess

    # Define Parameters:
    params = Parameters()
    params.add("f0", value=f0_first_guess, vary=True)
    params.add("qr", value=qr_guess, vary=True, min=0)
    params.add("qc_real", value=qr_guess * 2, vary=True, min=0)
    params.add("qc_imag", value=100.0, vary=True)
    # Guesses background level as mean of s21.
    params.add("a", value=np.mean(np.abs(magnitude_array)), vary=True)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(magnitude_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    result_dict["qc"] = [
        abs(result_dict["qc_real"][0] ** 2 + 1j * result_dict["qc_imag"][0])
        / result_dict["qc_real"][0],
        np.sqrt(
            result_dict["qc_real"][1] ** 2
            + (
                2
                * result_dict["qc_imag"][1]
                * result_dict["qc_imag"][0]
                / result_dict["qc_real"][0]
            )
            ** 2
            + result_dict["qc_real"][1]
            * result_dict["qc_imag"][0] ** 2
            / result_dict["qc_real"][0] ** 2
        ),
    ]
    qi = 1 / ((1 / result_dict["qr"][0]) - (1 / result_dict["qc"][0]))
    result_dict["qi"] = [
        qi,
        np.sqrt(
            qi**4
            * (
                result_dict["qr"][1] ** 2 / result_dict["qr"][0] ** 4
                + result_dict["qc"][1] ** 2 / result_dict["qc"][0] ** 4
            )
        ),
    ]

    f_array = np.linspace(
        start=frequency_array[start_index],
        stop=frequency_array[end_index],
        num=1000000,
    )

    lorentzian_fit = skewed_lorentzian(
        frequency_array=f_array,
        f0=result_dict["f0"][0],
        qr=result_dict["qr"][0],
        qc_real=result_dict["qc_real"][0],
        qc_imag=result_dict["qc_imag"][0],
        a=result_dict["a"][0],
    )

    if plot_graph:

        if plot_dB:
            y_label = "S21 Magnitude (dB)"
            magnitude_array = 20 * np.log10(magnitude_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        if not plot_dB:
            y_label = "S21 Magnitude"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            magnitude_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nqr=%0.2e\nqc=%0.2e\nQi=%0.2e\nf0=%0.2e"
            % (
                result_dict["qr"][0],
                result_dict["qc"][0],
                result_dict["qi"][0],
                result_dict["f0"][0],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    result_dict["fit_f_array"] = f_array
    result_dict["fit_array"] = lorentzian_fit

    return result_dict


########################################################################################################################


def lorentzian_s21(
    frequency_array: np.array, qr: float, qc: float, f0: float, amplitude=1.0
) -> np.array:
    """
    Function defining the model of a lorentzian resonance in an S21 array.

    :param np.array frequency_array: Array of N frequency points.
    :param float qr: Resonator quality factor.
    :param float qc: Coupling quality factor.
    :param float f0: Resonant frequency.
    :param float amplitude: Amplitude/ background level.
    """
    x = (frequency_array - f0) / f0
    a = 2.0 * qr * x
    real_s21 = amplitude - (qr / qc) * (1.0 / (a**2 + 1.0))
    imaginary_s21 = (qr / qc) * (a / (a**2 + 1.0))
    magnitude_s21 = np.sqrt(real_s21**2 + imaginary_s21**2)
    return magnitude_s21


def fit_lorentzian_s21(
    frequency_array: np.array,
    s21_array: np.array,
    qr_guess: float,
    qc_guess: float,
    plot_graph=True,
    plot_dB=True,
    plot_title="",
) -> dict:
    """
    Function to fit qr, qc and f0 to a resonance in S21.\n
    Returns fit parameters as a list: [qr, qc, f0, A].

    :param np.array frequency_array: Array of N frequency points.
    :param np.array s21_array: Complex array of N S21 data points.
    :param float qr_guess: Guess of resonators quality factor.
    :param float Qr_guess: Guess of coupling quality factor.
    :param bool plot_graph: Boolean command to plot a graph of the fit.
    :param bool plot_dB: Boolean command to plot S21 in dB.

    """

    s21_mag_array = np.abs(s21_array)

    f0_index = np.argmin(s21_mag_array)  # index of minimum point.
    resonance_data_points = (
        300  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(lorentzian_s21)

    # Define Parameters:
    params = Parameters()
    params.add("qc", value=qc_guess, min=0, vary=True)
    params.add("qr", value=qr_guess, expr="qc")
    params.add(
        "f0", value=frequency_array[np.argmin(s21_mag_array)], min=0, vary=True
    )  # Guesses f0 is min of s21
    params.add(
        "amplitude", value=np.mean(np.abs(s21_array)), vary=True
    )  # Guesses background level as mean of s21.

    result = l_model.fit(
        data=np.abs(s21_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    ideal = result.best_fit

    best_fit_parameters = {
        "qr": result.best_values["qr"],
        "qc": result.best_values["qc"],
        "f0": result.best_values["f0"],
        "amplitude": result.best_values["amplitude"],
    }

    if plot_graph == True:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=10000,
        )

        lorentzian_fit = lorentzian_s21(
            frequency_array=f_array,
            qr=result.best_values["qr"],
            qc=result.best_values["qc"],
            f0=result.best_values["f0"],
            amplitude=result.best_values["amplitude"],
        )

        if plot_dB == True:
            y_label = "S21 Magnitude"
            s21_mag_array = 20 * np.log10(s21_mag_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        if plot_dB == False:
            y_label = "S21 Magnitude (dB)"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            s21_mag_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nqr=%0.2e\nqc=%0.2e\nf0=%0.2e"
            % (
                result.best_values["qr"],
                result.best_values["qc"],
                result.best_values["f0"],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.show()

    return best_fit_parameters


def fit_and_interpolate_polynomial(
    x_data: np.array, y_data: np.array, xi: float, degree: int, plot_graph: True
):
    """
    Function to fit a polynomial of a specified degree to x and y data and interpolate
    at a given value.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param float xi: x value at which to interpolate a y value.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.vlines(xi, np.min(y_data), np.max(y_data), color="k")
        plt.hlines(polynomial(xi), np.min(x_data), np.max(x_data), color="k")
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    return polynomial(xi)


def fit_linear_regression(
    x_data: np.ndarray,
    y_data: np.ndarray,
    plot_graph=True,
    plot_title="",
    x_label="X Data",
    y_label="Y Data",
):
    """
    Function to fit a linear regression to x and y data and return the scipy LinregressResult object. See
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html for attributes and methods.

    :param x_data: X data to fit to.
    :param y_data: Y data to fit to.
    :param plot_graph: Boolean to
    :param x_label: Label for x axis of plot.
    :param y_label: Label for y axis of plot.
    :return: result: LinregressResult instance. The return value is an object with the following attributes:
        slope: float
            Slope of the regression line.
        intercept: float
            Intercept of the regression line.
        rvalue: float
            The Pearson correlation coefficient. The square of ``rvalue``
            is equal to the coefficient of determination.
        pvalue: float
            The p-value for a hypothesis test whose null hypothesis is
            that the slope is zero, using Wald Test with t-distribution of
            the test statistic. See `alternative` above for alternative
            hypotheses.
        stderr: float
            Standard error of the estimated slope (gradient), under the
            assumption of residual normality.
        intercept_stderr: float
            Standard error of the estimated intercept, under the assumption
            of residual normality.
    """

    result = stats.linregress(x_data, y_data)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", label="Data")
        plt.plot(
            x_data,
            result.intercept + result.slope * x_data,
            color="r",
            label="y = mx + c fit with:\nm=%.2e\nc=%.2e\nR$^2$=%.2e\n$\sigma_m$=%.2e\n$\sigma_c$=%.2e"
            % (
                result.slope,
                result.intercept,
                result.rvalue**2,
                result.stderr,
                result.intercept_stderr,
            ),
        )
        plt.title(plot_title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.legend()
        plt.show()

    return result
