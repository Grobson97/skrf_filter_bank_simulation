import skrf as rf
import numpy as np
from scipy import interpolate


def get_media_properties(microstrip_width: float, dielectric_thickness: float) -> tuple:
    """
    Loads the microstripDataArrays.npz file and extract the data arrays to then 2d interpolate the
    characteristic impedance, z0 and effective permittivity for a given microstrip_width and dielectricThickness.

    :param microstrip_width: Width of microstrip, use metres
    :param dielectric_thickness: Thickness of dielectric, use metres

    """

    # NOTE: If running test scripts, add ../ to the start of the data file string below:
    try:
        data_file = np.load("model_data/microstrip_data_arrays.npz")
    except:
        data_file = np.load("../model_data/microstrip_data_arrays.npz")
    microstrip_width_array = data_file["microstrip_width_array"]
    dielectric_thickness_array = data_file["dielectric_thickness_array"]
    mean_port_z0_array = data_file["mean_port_z0_array"]
    mean_port_permittivity_array = data_file["mean_port_permittivity_array"]

    # Interpolate data for given width and thickness:
    z0_value = interpolate.griddata(
        (microstrip_width_array, dielectric_thickness_array),
        mean_port_z0_array,
        (microstrip_width, dielectric_thickness),
    )

    effective_permittivity = interpolate.griddata(
        (microstrip_width_array, dielectric_thickness_array),
        mean_port_permittivity_array,
        (microstrip_width, dielectric_thickness),
    )

    return z0_value, effective_permittivity


def calculate_gamma(
    frequency_band: rf.Frequency, effective_permittivity: float, loss_tangent: float
) -> np.ndarray:
    """Calculates the array of propagation constants and gamma, of the microstrip transmission line
    at each frequency in the frequency_band.f.

    :param frequency_band: Defines the frequency band
    :param effective_permittivity: Effective permittivity for current microstrip parameters
    :param loss_tangent: Loss tangent of the media dielectric
    :returns: Array of propagation constants and gamma, of the microstrip transmission line at each frequency in the
        frequency_band.f.
    """
    v = 2.99e8 / np.sqrt(effective_permittivity)
    alpha = (np.pi * frequency_band.f / v) * loss_tangent
    beta = 2 * np.pi * frequency_band.f / v

    gamma = alpha + 1j * beta

    return gamma


def create_media(
    frequency_band: rf.Frequency,
    microstrip_width: float,
    loss_tangent: float,
    dielectric_thickness: float,
) -> rf.media.Media:
    """Creates and returns an instance of the rf.Media.DefinedGammaZ0 class for the defined frequency_band to
    represent
    a microstrip media of the given width and dielectric loss tangent.
    The media Z0 and gamma arrays are obtained by interpolating the port Z0 and effective_permittivity from sonnet
    simulations output files of inverted microstrip transmission line for different width microstrips and
    thicknesses of SiN.

    :param frequency_band: Start and stop must be within the frequency bounds of the sonnet simulation data
    :param microstrip_width: Width of the microstrip transmission line. Must be in units of metres
    :param loss_tangent: Loss tangent of dielectric
    :param dielectric_thickness: Thickness of dielectric layer

    """

    z0, effective_permittivity = get_media_properties(
        microstrip_width, dielectric_thickness
    )

    gamma_array = calculate_gamma(
        frequency_band=frequency_band,
        effective_permittivity=effective_permittivity,
        loss_tangent=loss_tangent,
    )

    microstrip_media = rf.media.DefinedGammaZ0(
        frequency=frequency_band, z0=z0, gamma=gamma_array
    )

    return microstrip_media
