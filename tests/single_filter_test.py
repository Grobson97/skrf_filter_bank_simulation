import util.filter_analysis_tools as filter_analysis_tools
import util.media_tools as media_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters

from util import touchstone_tools
from util.models.series_filter import SeriesFilter


def create_shunt_filter_network(
    feedline_media: rf.media.Media,
    input_media: rf.media.Media,
    output_media: rf.media.Media,
    target_f0: float,
    resolution: int,
    dielectric_thickness: float,
):
    # Create notch filter with skrf

    feedline_in = feedline_media.line(d=100, unit="um")
    feedline_out = feedline_media.line(d=100, unit="um")

    series_filter = SeriesFilter(
        channel_number=0,
        input_media=input_media,
        resonator_media=input_media,
        output_media=output_media,
        target_f0=target_f0,
        resolution=resolution,
        dielectric_thickness=dielectric_thickness,
    )

    series_filter_network = series_filter.create_network()

    # Convert channel network into a shunted network. increases number of ports by 1:
    shunt_filter = feedline_media.shunt(series_filter_network, name="CH0")

    # renumber ports for the 3 port channel network has the following indices:
    # 0: feedline input
    # 1: feedline output
    # 2: channel output
    shunt_filter.renumber([0, 1, 2], [0, 2, 1])

    # connect input with 3 port shunt channel:
    filter_channel_network = rf.connect(feedline_in, 1, shunt_filter, 0)
    filter_channel_network = rf.connect(filter_channel_network, 1, feedline_out, 1)

    return filter_channel_network

def main():

    resolution = 100
    dielectric_thickness = 300e-9
    loss_tangent = 0.0
    target_f0 = 150e9

    ########################################################################################################################

    frequency_band = rf.Frequency(
        start=140e9, stop=160e9, unit="Hz", npoints=1001
    )

    microstrip_media_2um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.0e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss_tangent,
    )
    microstrip_media_2p5um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss_tangent,
    )
    microstrip_media_3um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=3.0e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss_tangent,
    )

    ####################################################################################################################

    filter_channel_network = create_shunt_filter_network(
        feedline_media=microstrip_media_2p5um,
        input_media=microstrip_media_2um,
        output_media=microstrip_media_3um,
        target_f0=target_f0,
        resolution=resolution,
        dielectric_thickness=dielectric_thickness,
    )

    plt.figure(figsize=(8, 6))
    plt.plot(filter_channel_network.f*1e-9, np.abs(filter_channel_network.s[:, 0, 0]) ** 2)
    plt.plot(filter_channel_network.f*1e-9, np.abs(filter_channel_network.s[:, 0, 1]) ** 2)
    plt.plot(filter_channel_network.f*1e-9, np.abs(filter_channel_network.s[:, 0, 2]) ** 2)
    plt.show()


if __name__ == "__main__":
    main()
