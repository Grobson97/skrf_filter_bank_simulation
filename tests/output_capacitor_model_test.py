import util.filter_analysis_tools as filter_analysis_tools
import util.media_tools as media_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters


def coupling_capacitor_s21(
    data_frequency_array: np.ndarray,
    capacitance: float,
    parasitic_inductance: float,
    dielectric_thickness: float,
    microstrip_width: float,
    length: float,
):

    # Define frequency band:
    frequency_band = rf.Frequency(
        start=data_frequency_array[0],
        stop=data_frequency_array[-1],
        unit="Hz",
        npoints=data_frequency_array.size,
    )

    microstrip_media = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=microstrip_width,
        loss_tangent=0.0,
        dielectric_thickness=dielectric_thickness,
    )
    output_media = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=microstrip_width + 1.0e-6,
        loss_tangent=0.0,
        dielectric_thickness=dielectric_thickness,
    )

    input_line = microstrip_media.line(d=length, unit="um")
    output_line = output_media.line(d=length, unit="um")

    # Define Circuit components
    capacitor = microstrip_media.capacitor(C=capacitance, name="C")
    parasitic_inductor = microstrip_media.inductor(
        L=parasitic_inductance, name="Parasitic L"
    )

    coupling_capacitor_network = (
        input_line**parasitic_inductor**capacitor**parasitic_inductor**output_line
    )
    coupling_capacitor_network.renormalize(
        [
            complex(50, 0),
            complex(50, 0),
        ]
    )

    return coupling_capacitor_network.s_db[:, 0, 1]


def main():
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\skrf_model_components\coupling_capacitor\output_cap_width_sweep_1um_to_30um"
    file_name = "output_cap_width_sweep_param085.s2p"

    file_path = os.path.join(directory, file_name)

    # Read current touchstone file
    touchstone_file = rf.Touchstone(file_path)
    # Get touchstone variables from comments
    variables = touchstone_file.get_comment_variables()

    # Extract variables from header
    capacitor_width = float(variables["cap_width"][0])
    dielectric_thickness = float(variables["dielectric_thickness"][0]) * 1e-6
    microstrip_width = float(variables["input_width"][0]) * 1e-6
    print(capacitor_width)
    print(dielectric_thickness)

    print(capacitor_width)

    # Renormalise port impedances.
    network = rf.Network(file_path)
    frequency = network.f

    # Fit to parasitic capacitance to data:
    # Define model:
    model = Model(coupling_capacitor_s21)

    def fit_to_model(capacitance_guess):
        # Define Parameters:
        params = Parameters()
        params.add("capacitance", value=capacitance_guess, vary=True)
        params.add("parasitic_inductance", value=1e-10, min=0, vary=True)
        params.add("length", value=4, vary=False)
        params.add("microstrip_width", value=microstrip_width, vary=False)
        params.add("dielectric_thickness", value=dielectric_thickness, vary=False)

        result = model.fit(
            network.s_db[:, 0, 1],
            params,
            data_frequency_array=network.f,
        )

        return result

    capacitance_guess = 1e-15
    result = fit_to_model(capacitance_guess=capacitance_guess)
    attempts = 1
    while np.mean(np.abs(result.residual)) > 1e-2:
        print(f"    Attempts: {attempts}")
        capacitance_guess += 1e-15
        result = fit_to_model(capacitance_guess=capacitance_guess)
        attempts += 1
        if attempts > 2:
            break

    if attempts > 2:
        print(f"No suitable solution for {file_path}")

    ideal = result.best_fit
    fit_capacitance = result.best_values["capacitance"]
    fit_parasitic_inductance = result.best_values["parasitic_inductance"]

    plt.figure(figsize=(8, 6))
    plt.plot(network.f * 1e-9, network.s_db[:, 0, 1], linewidth=5, label="Sonnet")
    plt.plot(
        network.f * 1e-9,
        ideal,
        color="k",
        linestyle="--",
        label=f"Skrf model with:\nC={fit_capacitance:.3E}F\nL={fit_parasitic_inductance:.3E}H",
    )
    # plt.plot(network.f * 1e-9, coupling_cap_s21, color="k", linestyle="--", label=f"Skrf Model - Without L")
    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("S21 (dB)", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(fontsize=12)
    plt.show()


if __name__ == "__main__":
    main()
