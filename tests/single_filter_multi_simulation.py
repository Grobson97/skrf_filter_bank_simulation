import util.filter_analysis_tools as filter_analysis_tools
import util.media_tools as media_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters

from util import touchstone_tools
from util.models.series_filter import SeriesFilter


def create_shunt_filter_network(
    feedline_media: rf.media.Media,
    input_media: rf.media.Media,
    output_media: rf.media.Media,
    target_f0: float,
    resolution: int,
    dielectric_thickness: float,
):
    # Create notch filter with skrf

    feedline_in = feedline_media.line(d=100, unit="um")
    feedline_out = feedline_media.line(d=100, unit="um")

    series_filter = SeriesFilter(
        channel_number=0,
        input_media=input_media,
        resonator_media=input_media,
        output_media=output_media,
        target_f0=target_f0,
        resolution=resolution,
        dielectric_thickness=dielectric_thickness,
    )

    series_filter_network = series_filter.create_network()

    # Convert channel network into a shunted network. increases number of ports by 1:
    shunt_filter = feedline_media.shunt(series_filter_network, name="CH0")

    # renumber ports for the 3 port channel network has the following indices:
    # 0: feedline input
    # 1: feedline output
    # 2: channel output
    shunt_filter.renumber([0, 1, 2], [2, 0, 1])

    # connect input with 3 port shunt channel:
    filter_channel_network = rf.connect(feedline_in, 1, shunt_filter, 0)
    feedline_out = feedline_out ** feedline_media.match()
    filter_channel_network = rf.connect(filter_channel_network, 1, feedline_out, 0)

    return filter_channel_network

def main():

    resolution_array = np.array([100, 200, 300])
    dielectric_thickness = 300e-9
    loss_tangent = 1e-3

    target_f0_array = np.linspace(120e9, 180e9, 13)

    ########################################################################################################################

    frequency_band = rf.Frequency(
        start=100e9, stop=200e9, unit="Hz", npoints=1001
    )

    microstrip_media_2um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.0e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss_tangent,
    )
    microstrip_media_2p5um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss_tangent,
    )
    microstrip_media_3um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=3.0e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss_tangent,
    )

    ####################################################################################################################

    full_resolution_array = []
    full_target_f0_array = []
    fit_f0_array = []
    fit_qr_array = []
    max_power_array = []

    resolution = resolution_array[0]
    plt.figure(figsize=(8, 6))
    for resolution in resolution_array:
        for count, target_f0 in enumerate(target_f0_array):
            filter_channel_network = create_shunt_filter_network(
                feedline_media=microstrip_media_2p5um,
                input_media=microstrip_media_2um,
                output_media=microstrip_media_3um,
                target_f0=target_f0,
                resolution=resolution,
                dielectric_thickness=dielectric_thickness,
            )

            skrf_fit = filter_analysis_tools.fit_filter_s31_lorentzian(
                frequency_array=filter_channel_network.f,
                data_array=np.abs(filter_channel_network.s[:, 0, 1]) ** 2,
                q_filter_guess=resolution,
                qi_guess=10000,
                f0_guess=filter_channel_network.f[
                    np.argmax(np.abs(filter_channel_network.s[:, 0, 1]) ** 2)
                ],
                normalise=False,
                plot_graph=False,
                plot_db=False,
            )

            plt.plot(filter_channel_network.f*1e-9, np.abs(filter_channel_network.s[:, 0, 1]) ** 2)


            fit_f0 = skrf_fit["f0"][0]
            fit_qr = skrf_fit["q_filter"][0]
            max_power = np.max(np.abs(filter_channel_network.s[:, 0, 1]) ** 2)

            full_resolution_array.append(resolution)
            full_target_f0_array.append(target_f0)
            fit_f0_array.append(fit_f0)
            fit_qr_array.append(fit_qr)
            max_power_array.append(max_power)


            print(f"Filter {count}/{target_f0_array.size * resolution_array.size} Complete")

    plt.show()

    full_resolution_array = np.array(full_resolution_array)
    full_target_f0_array = np.array(full_target_f0_array)
    fit_f0_array = np.array(fit_f0_array)
    fit_qr_array = np.array(fit_qr_array)
    max_power_array = np.array(max_power_array)


    r100_indices = np.where(full_resolution_array == 100)[0]
    r200_indices = np.where(full_resolution_array == 200)[0]
    r300_indices = np.where(full_resolution_array == 300)[0]

    plt.figure(figsize=(8, 6))
    plt.plot(full_target_f0_array[r100_indices]*1e-9, fit_f0_array[r100_indices]*1e-9, linestyle="none", marker="x", label="R100", fillstyle="none", markersize=12, mew=2)
    plt.plot(full_target_f0_array[r200_indices]*1e-9, fit_f0_array[r200_indices]*1e-9, linestyle="none", marker="+", label="R200", fillstyle="none", markersize=12, mew=2)
    plt.plot(full_target_f0_array[r300_indices]*1e-9, fit_f0_array[r300_indices]*1e-9, linestyle="none", marker="_", label="R300", fillstyle="none", markersize=12, mew=2)
    plt.plot(target_f0_array*1e-9, target_f0_array*1e-9, linestyle="--", color="k", label="y=x")
    plt.xlabel("Target F0 (GHz)", fontsize=16)
    plt.ylabel("Model F0 (GHz)", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(fontsize=12)
    plt.show()

    plt.figure(figsize=(8, 6))
    plt.plot(fit_f0_array[r100_indices]*1e-9, fit_qr_array[r100_indices], linestyle="none", marker="o", label="R100 Model")
    plt.plot(fit_f0_array[r200_indices]*1e-9, fit_qr_array[r200_indices], linestyle="none", marker="o", label="R200 Model")
    plt.plot(fit_f0_array[r300_indices]*1e-9, fit_qr_array[r300_indices], linestyle="none", marker="o", label="R300 Model")
    plt.hlines(y=100, xmin=120, xmax=180, linestyles="--", color="k", label="R=100")
    plt.hlines(y=200, xmin=120, xmax=180, linestyles=":", color="k", label="R=200")
    plt.hlines(y=300, xmin=120, xmax=180, linestyles="-.", color="k", label="R=300")
    plt.xlabel("Model (GHz)", fontsize=16)
    plt.ylabel("Filter Q (GHz)", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(fontsize=12)
    plt.show()

    plt.figure(figsize=(8, 6))
    plt.plot(fit_f0_array[r100_indices]*1e-9, max_power_array[r100_indices], linestyle="none", marker="o", label="R100")
    plt.plot(fit_f0_array[r200_indices]*1e-9, max_power_array[r200_indices], linestyle="none", marker="o", label="R200")
    plt.plot(fit_f0_array[r300_indices]*1e-9, max_power_array[r300_indices], linestyle="none", marker="o", label="R300")
    plt.hlines(y=0.5, xmin=120, xmax=180, linestyles="--", color="k", label="Max efficiency")
    plt.xlabel("Model (GHz)", fontsize=16)
    plt.ylabel("Peak Power", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(fontsize=12)
    plt.show()


if __name__ == "__main__":
    main()
