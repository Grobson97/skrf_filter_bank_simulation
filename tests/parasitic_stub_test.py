import util.filter_analysis_tools as filter_analysis_tools
import util.media_tools as media_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters


def parasitic_stub_s21(
    data_frequency_array: np.ndarray,
    capacitance: float,
    length: float,
):

    # Define frequency band:
    frequency_band = rf.Frequency(
        start=data_frequency_array[0],
        stop=data_frequency_array[-1],
        unit="Hz",
        npoints=data_frequency_array.size,
    )

    microstrip_media = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        loss_tangent=0.0,
        dielectric_thickness=300e-9,
    )

    line = microstrip_media.line(d=length, unit="um")

    # Define Circuit components
    capacitor = microstrip_media.capacitor(C=capacitance, name="C")

    # Add a short after parasitic capacitor
    parasitic_cap_to_gnd = capacitor ** microstrip_media.short()
    parasitic_cap_to_gnd = microstrip_media.shunt(
        parasitic_cap_to_gnd, name="Parasitic C"
    )
    parasitic_line_network = line ** parasitic_cap_to_gnd ** line

    parasitic_line_network.renormalize(
        [
            complex(50, 0),
            complex(50, 0),
        ]
    )

    return parasitic_line_network.s_db[:, 0, 1]


def main():
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\skrf_model_components\stub"
    file_name = "parasitic_stub_param1.s2p"

    file_path = os.path.join(directory, file_name)

    network = rf.Network(file_path)
    frequency = network.f

    # Fit to parasitic capacitance to data:
    # Define model:
    model = Model(parasitic_stub_s21)

    def fit_to_model(capacitance_guess):
        # Define Parameters:
        params = Parameters()
        params.add("capacitance", value=capacitance_guess, vary=True)
        params.add("length", value=100, vary=False)

        result = model.fit(
            network.s_db[:, 0, 1],
            params,
            data_frequency_array=network.f,
        )

        return result

    capacitance_guess = 1.296e-14
    result = fit_to_model(capacitance_guess=capacitance_guess)
    attempts = 1
    while np.mean(np.abs(result.residual)) > 1e-2:
        print(f"    Attempts: {attempts}")
        capacitance_guess += 1e-15
        result = fit_to_model(capacitance_guess=capacitance_guess)
        attempts += 1
        if attempts > 4:
            break

    if attempts > 4:
        print(f"No suitable solution for {file_path}")

    ideal = result.best_fit
    fit_capacitance = result.best_values["capacitance"]
    print(fit_capacitance)

    # Define frequency band:
    frequency_band = rf.Frequency(
        start=network.f[0],
        stop=network.f[-1],
        unit="Hz",
        npoints=network.f.size,
    )

    microstrip_media = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        loss_tangent=0.0,
        dielectric_thickness=300e-9,
    )
    line = microstrip_media.line(d=200, unit="um")
    line.renormalize(50)

    plt.figure(figsize=(8, 6))
    plt.plot(network.f * 1e-9, network.s_db[:, 0, 1], linewidth=5, label="Sonnet")
    plt.plot(line.f * 1e-9, line.s_db[:, 0, 1], label="Simple Skrf microstrip")
    plt.plot(
        network.f * 1e-9,
        ideal,
        color="k",
        linestyle="--",
        label=f"Skrf model with:\nC={fit_capacitance:.3E}F",
    )
    # plt.plot(network.f * 1e-9, coupling_cap_s21, color="k", linestyle="--", label=f"Skrf Model - Without L")
    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("S21 (dB)", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(fontsize=12)
    plt.show()


if __name__ == "__main__":
    main()
