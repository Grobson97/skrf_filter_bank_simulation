import util.filter_analysis_tools as filter_analysis_tools
import util.media_tools as media_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters

from util import touchstone_tools
from util.models.series_filter import SeriesFilter


def main():
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\skrf_model_components\filter\r100"
    file_name = "r100_2um_ppcap_filters_2_with_membrane_layers_param2.s3p"

    to_port = 3
    resolution = 100
    dielectric_thickness = 300e-9
    loss = 0.0

    ########################################################################################################################

    # Load Sonnet Data:

    file_path = os.path.join(directory, file_name)
    # Read current touchstone file
    touchstone_file = rf.Touchstone(file_path)
    # Get touchstone variables from comments
    variables = touchstone_file.get_comment_variables()

    # Extract variables from header
    resonator_length = float(variables["resonator_length"][0]) * 1e-6
    input_width = float(variables["input_cap"][0]) * 1e-6
    output_cap = float(variables["output_cap"][0]) * 1e-6

    # Extract mean port impedance and E effective
    input_port_z0_result = touchstone_tools.get_mean_port_impedance(
        file_path=file_path, port_number="1"
    )
    filter_out_port_z0_result = touchstone_tools.get_mean_port_impedance(
        file_path=file_path, port_number="3"
    )
    input_mean_port_z0 = complex(input_port_z0_result[0], input_port_z0_result[1])
    filter_out_port_z0 = complex(
        filter_out_port_z0_result[0], filter_out_port_z0_result[1]
    )

    print(f"Sonnet Resonator Length = {resonator_length*1e6}um")
    print(f"Sonnet Input Cap Width = {input_width*1e6}um")
    print(f"Sonnet Output Cap Width = {output_cap*1e6}um")

    # Renormalise port impedances.
    sonnet_network = rf.Network(file_path)
    sonnet_network.renormalize(
        [input_mean_port_z0, input_mean_port_z0, filter_out_port_z0]
    )

    sonnet_fit = filter_analysis_tools.fit_filter_s31_lorentzian(
        frequency_array=sonnet_network.f,
        data_array=np.abs(sonnet_network.s[:, 0, to_port - 1]) ** 2,
        q_filter_guess=resolution,
        qi_guess=10000,
        f0_guess=sonnet_network.f[
            np.argmax(np.abs(sonnet_network.s[:, 0, to_port - 1]) ** 2)
        ],
        normalise=False,
        plot_graph=True,
        plot_db=False,
    )

    sonnet_f0 = sonnet_fit["f0"][0]
    print(sonnet_f0)

    ####################################################################################################################

    # Create notch filter with skrf:

    frequency_band = rf.Frequency(
        start=sonnet_network.f[0], stop=sonnet_network.f[-1], unit="Hz", npoints=1001
    )

    microstrip_media_2um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.0e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss,
    )
    microstrip_media_2p5um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss,
    )
    microstrip_media_3um = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=3.0e-6,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=loss,
    )

    feedline_in = microstrip_media_2p5um.line(d=100, unit="um")
    feedline_out = microstrip_media_2p5um.line(d=100, unit="um")

    series_filter = SeriesFilter(
        channel_number=0,
        input_media=microstrip_media_2um,
        resonator_media=microstrip_media_2um,
        output_media=microstrip_media_3um,
        target_f0=sonnet_f0,
        resolution=resolution,
        dielectric_thickness=dielectric_thickness,
    )

    series_filter_network = series_filter.create_network()

    # Convert channel network into a shunted network. increases number of ports by 1:
    shunt_filter = microstrip_media_2p5um.shunt(series_filter_network, name="CH0")

    # renumber ports for the 3 port channel network has the following indices:
    # 0: feedline input
    # 1: feedline output
    # 2: channel output
    shunt_filter.renumber([0, 1, 2], [2, 0, 1])

    # connect input with 3 port shunt channel:
    filter_channel_network = rf.connect(feedline_in, 1, shunt_filter, 0)

    if to_port == 2:
        filter_channel_network = rf.connect(
            filter_channel_network, 2, microstrip_media_3um.match(), 0
        )
        filter_channel_network = filter_channel_network**feedline_out

    if to_port == 3:
        feedline_out = feedline_out ** microstrip_media_2p5um.match()
        filter_channel_network = rf.connect(filter_channel_network, 1, feedline_out, 0)

    plt.figure(figsize=(8, 6))
    plt.plot(
        sonnet_network.f * 1e-9,
        np.abs(sonnet_network.s[:, 0, 2]) ** 2,
        linewidth=5,
        label="Sonnet",
    )
    plt.plot(
        frequency_band.f * 1e-9,
        np.abs(filter_channel_network.s[:, 0, 1]) ** 2,
        label="Filter Channel",
        linestyle="--",
        color="k",
    )
    # plt.plot(network.f * 1e-9, coupling_cap_s21, color="k", linestyle="--", label=f"Skrf Model - Without L")
    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("S31 Power", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(fontsize=12)
    plt.show()

    skrf_fit = filter_analysis_tools.fit_filter_s31_lorentzian(
        frequency_array=filter_channel_network.f,
        data_array=np.abs(filter_channel_network.s[:, 0, 1]) ** 2,
        q_filter_guess=resolution,
        qi_guess=10000,
        f0_guess=filter_channel_network.f[
            np.argmax(np.abs(filter_channel_network.s[:, 0, 1]) ** 2)
        ],
        normalise=False,
        plot_graph=True,
        plot_db=False,
    )


if __name__ == "__main__":
    main()
