import complete_tolerancing.complete_tolerancing_tools as complete_tolerancing_tools
import skrf as rf
import numpy as np
import matplotlib.pyplot as plt

# Define readout frequency band:
from util import filter_bank_tools

frequency_band = rf.Frequency(start=100, stop=200, unit="GHz", npoints=1001)
oversampling = 1.6
resolution = 300
frequency_max = 180
frequency_min = 120
filter_spacing = 0.75

number_of_channels = round(
    oversampling * resolution * np.log(frequency_max / frequency_min)
)
target_f0_array = (
    filter_bank_tools.create_target_f0_array(
        frequency_min, frequency_max, number_of_channels
    )
    * 1e9
)

# Block to create a thicknessArray and etchErrorArray using MUSCAT data based on position.
position_array = np.linspace(0, number_of_channels, number_of_channels)
nominal_dielectric_thickness = 300e-9
dielectric_thickness_array = np.full(
    position_array.shape, fill_value=nominal_dielectric_thickness
)
etch_error_array = np.zeros(position_array.shape)
dielectric_thickness_array_with_error = (
    dielectric_thickness_array * (1-complete_tolerancing_tools.thickness_error(position_array, filter_spacing=filter_spacing))
)
etch_error_array = complete_tolerancing_tools.etch_error(position_array)

plt.figure(figsize=(8, 6))
plt.plot(dielectric_thickness_array * 1e9, label="Nominal")
plt.plot(dielectric_thickness_array_with_error * 1e9, label="With error")
plt.legend()
plt.xlabel("Filter Index")
plt.ylabel("SiN thickness (nm)")
plt.show()

plt.figure(figsize=(8, 6))
plt.plot(etch_error_array * 1e6)
plt.legend()
plt.xlabel("Filter Index")
plt.ylabel("Etch error (um)")
plt.show()
