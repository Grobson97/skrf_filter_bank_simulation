import skrf as rf
import numpy as np
import matplotlib.pyplot as plt

from util.models.filter_bank import FilterBank

import util.filter_bank_tools as filter_bank_tools
import complete_tolerancing.complete_tolerancing_tools as complete_tolerancing_tools


def main():

    start = 160
    stop = 140
    number_of_points = 1001
    oversampling = 1.6
    loss_tangent = 0.0
    filter_spacing = 0.5
    dielectric_thickness = 300e-9
    resolution = 100
    frequency_max = 150
    frequency_min = 145

    # Define readout frequency band:
    frequency_band = rf.Frequency(
        start=start, stop=stop, unit="GHz", npoints=number_of_points
    )

    number_of_channels = round(
        oversampling * resolution * np.log(frequency_max / frequency_min)
    )
    target_f0_array = (
        filter_bank_tools.create_target_f0_array(
            frequency_min, frequency_max, number_of_channels
        )
        * 1e9
    )

    filter_bank_s_params = filter_bank_tools.get_filter_bank_s_params(
        frequency_band=frequency_band,
        target_f0_array=target_f0_array,
        resolution=resolution,
        etch_error_array=np.zeros(target_f0_array.shape),
        nominal_dielectric_thickness=dielectric_thickness,
        dielectric_thickness_array=np.full(target_f0_array.shape, dielectric_thickness),
        loss_tangent=loss_tangent,
        filter_spacing=filter_spacing,
        oversampling=oversampling,
    )

    plt.figure(figsize=(8, 6))
    # Begin counting total transmission.
    total_power = np.zeros(filter_bank_s_params[0].shape)
    channel_total_power = np.zeros(filter_bank_s_params[0].shape)
    for index, s_param in enumerate(np.abs(filter_bank_s_params)):
        if index == 0 or index == 1:
            total_power = total_power + s_param ** 2
            plt.plot(frequency_band.f, s_param ** 2, color="k", linestyle=":")
            continue
        channel_total_power = channel_total_power + s_param ** 2
        total_power = total_power + s_param ** 2
        plt.plot(frequency_band.f, s_param ** 2)

    plt.plot(frequency_band.f, total_power, color="k", linestyle="--")
    plt.plot(frequency_band.f, channel_total_power, color="k")

    plt.show()


if __name__ == "__main__":
    main()
