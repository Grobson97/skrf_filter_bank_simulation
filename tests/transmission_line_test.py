import numpy as np
import skrf as rf
import matplotlib.pyplot as plt
import os

import util.touchstone_tools as touchstone_tools
import util.media_tools as media_tools


def main():

    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\skrf_model_components\microstrip\50um_long_deembedded"
    file_names = ["microstrip_with_membranes_param057.s2p"]

    plt.figure(figsize=(8, 6))

    for count, data_filename in enumerate(file_names):
        data_file_path = os.path.join(directory, data_filename)

        # Read current touchstone file
        touchstone_file = rf.Touchstone(data_file_path)
        # Get touchstone variables from comments
        variables = touchstone_file.get_comment_variables()
        # Extract variables from header
        microstrip_width = float(variables["width"][0])
        dielectric_thickness = float(variables["dielectric_thickness"][0])

        # Extract mean port impedance and E effective
        mean_port_z0_result = touchstone_tools.get_mean_port_impedance(
            file_path=data_file_path, port_number="1"
        )
        mean_port_z0 = complex(mean_port_z0_result[0], mean_port_z0_result[1])
        mean_port_permittivity = touchstone_tools.get_mean_port_permittivity(
            file_path=data_file_path, port_number="1"
        )

        # Renormalise port impedances.
        network = rf.Network(data_file_path)
        frequency_band = rf.Frequency(
            start=network.f[0], stop=network.f[-1], unit="Hz", npoints=1001
        )

        gamma_array = media_tools.calculate_gamma(
            frequency_band=frequency_band,
            effective_permittivity=mean_port_permittivity,
            loss_tangent=0.0,
        )
        microstrip_media = rf.media.DefinedGammaZ0(
            frequency=frequency_band, z0=mean_port_z0, gamma=gamma_array
        )

        model_line = microstrip_media.line(d=50, unit="um")
        model_line.renormalize(50)

        plt.plot(
            network.f * 1e-9,
            network.s_db[:, 0, 1],
            linewidth=4,
            label=f"w={microstrip_width}um, t={dielectric_thickness*1e3}nm",
        )
        plt.plot(
            model_line.f * 1e-9, model_line.s_db[:, 0, 1], color="k", linestyle="--"
        )

    plt.plot([], [], color="k", linestyle="--", label="Skrf Model")
    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("S21 (dB)", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(fontsize=12)
    plt.show()


if __name__ == "__main__":
    main()
