from util.models.coupling_capacitor import CouplingCapacitor
import util.media_tools as media_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters


def main():
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\skrf_model_components\coupling_capacitor\input_cap_width_sweep_1um_to_30um"
    file_name = "input_cap_width_sweep_param112.s2p"

    file_path = os.path.join(directory, file_name)

    # Read current touchstone file
    touchstone_file = rf.Touchstone(file_path)
    # Get touchstone variables from comments
    variables = touchstone_file.get_comment_variables()

    # Extract variables from header
    capacitor_width = float(variables["cap_width"][0]) * 1e-6
    dielectric_thickness = float(variables["dielectric_thickness"][0]) * 1e-6
    microstrip_width = float(variables["width"][0]) * 1e-6

    # Renormalise port impedances.
    network = rf.Network(file_path)
    frequency_band = rf.Frequency(
        start=network.f[0], stop=network.f[-1], unit="Hz", npoints=1001
    )

    microstrip_media = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=microstrip_width,
        dielectric_thickness=dielectric_thickness,
        loss_tangent=0.0,
    )

    # Create coupling capacitor:
    coupling_capacitor = CouplingCapacitor(
        input_media=microstrip_media,
        output_media=microstrip_media,
        capacitor_width=capacitor_width,
        dielectric_thickness=dielectric_thickness,
        input_capacitor=True,
    )
    coupling_capacitor_network = coupling_capacitor.create_network()
    coupling_capacitor_network.renormalize(50)

    plt.figure(figsize=(8, 6))
    plt.plot(network.f * 1e-9, network.s_db[:, 0, 1], linewidth=5, label="Sonnet")
    plt.plot(
        coupling_capacitor_network.f * 1e-9,
        coupling_capacitor_network.s_db[:, 0, 1],
        color="k",
        linestyle="--",
        label=f"Skrf model",
    )
    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("S21 (dB)", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(fontsize=12)
    plt.show()


if __name__ == "__main__":
    main()
