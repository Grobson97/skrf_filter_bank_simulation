import util.filter_analysis_tools as filter_analysis_tools
import util.media_tools as media_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters

from util import filter_bank_tools, filter_tools


def create_parasitic_cascade(
        frequency_band: rf.Frequency,
        microstrip_media: rf.media.Media,
        resolution: float,
        oversampling: float,
        spacing: float,
        target_f0_max: float,
        target_f0_min: float
):

    number_of_channels = round(
        oversampling * resolution * np.log(target_f0_max / target_f0_min)
    )
    target_f0_array = (
        filter_bank_tools.create_target_f0_array(
            target_f0_min, target_f0_max, number_of_channels
        )
    )

    input_line = microstrip_media.line(d=100, unit="um")

    # Define Circuit components
    capacitor = microstrip_media.capacitor(C=1.295896913918483e-14, name="C")

    # Add a short after parasitic capacitor
    parasitic_cap_to_gnd = capacitor ** microstrip_media.short()
    parasitic_cap_to_gnd = microstrip_media.shunt(
        parasitic_cap_to_gnd, name="Parasitic C"
    )

    network = input_line

    for target_f0 in target_f0_array:
        # Allow option to set spacing as a fixed value.
        if spacing < 1e-3:
            interconnection_length = spacing
        else:
            interconnection_length = filter_tools.get_resonator_length(resolution=resolution, target_f0=target_f0) * 2 * spacing
        network = network ** parasitic_cap_to_gnd ** microstrip_media.line(interconnection_length, unit="m")

    return network


def main():

    frequency_start = 50e9
    frequency_stop = 300e9
    oversampling = 0.6
    resolution = 100
    target_f0_min = 120e9
    target_f0_max = 180e9
    spacing_array = [0.25, 0.5, 0.75, 250e-6]

    # Define frequency band:
    frequency_band = rf.Frequency(
        start=frequency_start,
        stop=frequency_stop,
        unit="Hz",
        npoints=10001,
    )

    microstrip_media = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=2.5e-6,
        loss_tangent=0.0,
        dielectric_thickness=300e-9,
    )
    plt.figure(figsize=(8, 6))

    min_s21 = 0
    for spacing in spacing_array:

        network = create_parasitic_cascade(
            frequency_band=frequency_band,
            microstrip_media=microstrip_media,
            resolution=resolution,
            oversampling=oversampling,
            spacing=spacing,
            target_f0_max=target_f0_max,
            target_f0_min=target_f0_min,
        )
        print("Cascade Complete")
        if spacing < 1e-3:
            plt.plot(network.f * 1e-9, network.s_db[:, 0, 1], label=f"{spacing:.1e}um", linestyle=":", color="darkgrey")
        else:
            plt.plot(network.f * 1e-9, network.s_db[:, 0, 1], label=f"{spacing}$\lambda$")


        current_min_s21 = np.min(network.s_db[:, 0, 1])

        if current_min_s21 < min_s21:
            min_s21 = current_min_s21

    plt.vlines(x=[120, 180], ymin=min_s21, ymax=0, linestyles="--", color="k")
    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("S21 (dB)", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(title="Spacing", fontsize=12, title_fontsize=14)
    plt.show()


if __name__ == "__main__":
    main()
