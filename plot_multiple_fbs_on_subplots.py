import json

import matplotlib
import numpy as np
import click
import os
import matplotlib.pyplot as plt

from complete_tolerancing.model.FilterBankData import FilterBankData
import complete_tolerancing.complete_tolerancing_tools as complete_tolerancing_tools


def filter_bank_decoder(dictionary: dict):
    """
    Called for each dictionary in the JSON string to check if it can be converted to an object

    :param dictionary: The dictionary to be processed
    :return: The converted object or original dictionary
    """
    # Convert to complex numpy array
    if "__complex__" in dictionary:
        return np.array(dictionary["real"]) + 1j * np.array(dictionary["imag"])
    # Convert to numpy arrays
    if "__ndarray__" in dictionary:
        return np.array(dictionary["list"])
    return dictionary


def main():

    directory = r"simulated_data/temp"
    save_figure = False

    lst = os.listdir(directory)  # your directory path
    number_of_files = len(lst)

    figure, axes = plt.subplots(nrows=number_of_files, ncols=1, sharex=True, figsize=(10, 4*number_of_files))

    for count, filename in enumerate(os.listdir(directory)):
        file_path = os.path.join(directory, filename)

        with open(file_path, "r") as file:
            filter_bank_json = file.read()

        loss_tangent = float(filename[
            filename.find("loss") + 5 : filename.find("-data-points")
        ])
        resolution = filename[1:4]
        oversampling = filename[
                       filename.find("oversampling") + 13: filename.find("-loss")
                       ]
        filter_bank_dict = json.loads(filter_bank_json, object_hook=filter_bank_decoder)
        filter_bank_data = FilterBankData(**filter_bank_dict)

        colour_map = plt.get_cmap("gist_rainbow_r")
        colors = [
            colour_map(i)
            for i in np.linspace(0, 1, filter_bank_data.realistic_filter_bank_s_params.shape[0])
        ]

        # Begin counting total transmission.
        non_ideal_total_power = np.zeros(filter_bank_data.realistic_filter_bank_s_params[0].shape)
        non_ideal_channel_total_power = np.zeros(filter_bank_data.realistic_filter_bank_s_params[0].shape)

        for index, s_param in enumerate(np.abs(filter_bank_data.realistic_filter_bank_s_params)):
            if index == 0 or index == 1:
                non_ideal_total_power = non_ideal_total_power + s_param ** 2
                continue
            non_ideal_total_power = non_ideal_total_power + s_param ** 2
            non_ideal_channel_total_power = non_ideal_channel_total_power + s_param ** 2

            axes[count].plot(
                filter_bank_data.frequency_array * 1e-9,
                s_param ** 2,
                color=colors[index],
            )

        # Repeat for ideal filter-bank:
        ideal_total_power = np.zeros(filter_bank_data.ideal_filter_bank_s_params[0].shape)
        ideal_channel_total_power = np.zeros(filter_bank_data.ideal_filter_bank_s_params[0].shape)

        for index, s_param in enumerate(np.abs(filter_bank_data.ideal_filter_bank_s_params)):
            if index == 0 or index == 1:
                ideal_total_power = ideal_total_power + s_param ** 2
                continue

            ideal_total_power = ideal_total_power + s_param ** 2
            ideal_channel_total_power = ideal_channel_total_power + s_param ** 2

            if index == 2:
                label = "Ideal $|S_{i1}|^2$"
            else:
                label = ""
            axes[count].plot(
                filter_bank_data.frequency_array * 1e-9,
                s_param ** 2,
                linestyle="dotted",
                color="0.75",
                label=label,
            )

        axes[count].plot(
            filter_bank_data.frequency_array * 1e-9,
            non_ideal_channel_total_power,
            linestyle="-",
            color="k",
            label="Realistic $\\Sigma_{i=3}^{N+2} |S_{i1}|^2$",
        )

        axes[count].plot(
            filter_bank_data.frequency_array * 1e-9,
            ideal_channel_total_power,
            linestyle="-.",
            color="0.75",
            label="Ideal $\\Sigma_{i=3}^{N+2} |S_{i1}|^2$",
        )
        axes[count].vlines(
            [120, 180],
            ymin=0.0,
            ymax=1.0,
            color="r",
            linestyle="--",
            label="Target frequency bounds",
        )

        # plt.tick_params("both", labelsize=14)
        axes[count].set_ylabel("Power\nTransmission", fontsize=14)
        axes[count].text(0.79, 0.8, f"R={resolution}", fontsize=12, transform=axes[count].transAxes)
        # axes[count].text(0.79, 0.8, f"tan$\delta$={loss_tangent:.1E}", fontsize=12, transform=axes[count].transAxes)

    axes[-1].set_xlabel("Frequency (GHz)", fontsize=14)

    # plt.tick_params("both", labelsize=14)
    # plt.xlim(frequency_min, frequency_max)
    plt.subplots_adjust(
        top=0.93,
        hspace=0.1
    )
    handles, labels = axes[0].get_legend_handles_labels()
    figure.legend(
        handles=handles,
        labels=labels,
        loc="upper center",
        ncol=4,
    )
    # plt.legend(fontsize=14, loc="center right", bbox_to_anchor=(0.5, 1.5))
    plt.savefig("multi_fbs_subplots.png")
    plt.show()


if __name__ == "__main__":
    main()
