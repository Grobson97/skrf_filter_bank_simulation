import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate

data_file = np.load("../model_data/output_coupling_capacitor_data_arrays.npz")
cap_width_array = data_file["cap_width_array"]
dielectric_thickness_array = data_file["dielectric_thickness_array"]
capacitance_array = data_file["capacitance_array"]
parasitic_inductance_array = data_file["parasitic_inductance_array"]

x = dielectric_thickness_array * 1e9
y = cap_width_array * 1e6
z = capacitance_array * 1e12

fig = plt.figure()

grid_x, grid_y = np.meshgrid(x, y)

ax = plt.axes(projection="3d")
ax.plot_trisurf(x, y, z, cmap="viridis", edgecolor="none")
ax.set_title("")
ax.set_xlabel("SiN thickness (um)", fontsize=14)
ax.set_ylabel("Cap Width (um)", fontsize=14)
ax.set_zlabel("Capacitance (pF)", fontsize=14)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
ax.tick_params("z", labelsize=11)
plt.savefig("coupling_cap_C_surface.png", bbox_inches="tight", transparent="True")
plt.show()

y = dielectric_thickness_array * 1e9
x = cap_width_array * 1e6
z = parasitic_inductance_array * 1e12

fig = plt.figure()

grid_x, grid_y = np.meshgrid(x, y)

ax = plt.axes(projection="3d")
ax.plot_trisurf(x, y, z, cmap="viridis", edgecolor="none")
ax.set_title("")
ax.view_init(33, 26)
ax.set_ylabel("SiN thickness (um)", fontsize=14)
ax.set_xlabel("Cap Width (um)", fontsize=14)
ax.set_zlabel("Parasitic $L$ (pF)", fontsize=14, rotation=180)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
ax.tick_params("z", labelsize=11)
plt.savefig("coupling_cap_L_surface.png", bbox_inches="tight", transparent="True")
plt.show()

capacitance = interpolate.griddata(
    (cap_width_array, dielectric_thickness_array),
    capacitance_array,
    (10 * 1e-6, 0.3 * 1e-6),
)
print(capacitance)
