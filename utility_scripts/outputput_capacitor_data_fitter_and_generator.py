import util.media_tools as media_tools
import skrf as rf
import matplotlib.pyplot as plt
import numpy as np
import os
from lmfit import Model
from lmfit import Parameters


def coupling_capacitor_s21(
    data_frequency_array: np.ndarray,
    capacitance: float,
    parasitic_inductance: float,
    dielectric_thickness: float,
    microstrip_width: float,
    length: float,
):

    # Define frequency band:
    frequency_band = rf.Frequency(
        start=data_frequency_array[0],
        stop=data_frequency_array[-1],
        unit="Hz",
        npoints=data_frequency_array.size,
    )

    microstrip_media = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=microstrip_width,
        loss_tangent=0.0,
        dielectric_thickness=dielectric_thickness,
    )
    output_media = media_tools.create_media(
        frequency_band=frequency_band,
        microstrip_width=microstrip_width + 1.0e-6,
        loss_tangent=0.0,
        dielectric_thickness=dielectric_thickness,
    )

    input_line = microstrip_media.line(d=length, unit="um")
    output_line = output_media.line(d=length, unit="um")

    # Define Circuit components
    capacitor = microstrip_media.capacitor(C=capacitance, name="C")
    parasitic_inductor = microstrip_media.inductor(
        L=parasitic_inductance, name="Parasitic L"
    )

    coupling_capacitor_network = (
        input_line**parasitic_inductor**capacitor**parasitic_inductor**output_line
    )
    coupling_capacitor_network.renormalize(
        [
            complex(50, 0),
            complex(50, 0),
        ]
    )

    return coupling_capacitor_network.s_db[:, 0, 1]


directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Sonnet\SPT-SLIM\skrf_model_components\coupling_capacitor\output_cap_width_sweep_1um_to_30um"

fit_dictionary = {
    "dielectric_thickness": [],
    "capacitor_width": [],
    "capacitance": [],
    "parasitic_inductance": [],
}

for count, file in enumerate(os.listdir(directory)):
    filename = os.fsdecode(file)
    file_path = os.path.join(directory, filename)

    if ".s2p" in filename:
        # Read current touchstone file
        touchstone_file = rf.Touchstone(file_path)
        # Get touchstone variables from comments
        variables = touchstone_file.get_comment_variables()

        # Make string ccontaining file parameters:
        variables_string = ""
        for variable in variables:
            if variable == list(variables.keys())[-1]:
                variables_string += variable + "=" + str(variables[variable][0])
                continue
            variables_string += variable + "=" + str(variables[variable][0]) + ", "

        # Extract variables from header
        capacitor_width = float(variables["cap_width"][0]) * 1e-6
        dielectric_thickness = float(variables["dielectric_thickness"][0]) * 1e-6
        microstrip_width = float(variables["input_width"][0]) * 1e-6

        # Renormalise port impedances.
        network = rf.Network(file_path)
        frequency = network.f

        # Fit to parasitic capacitance to data:
        # Define model:
        model = Model(coupling_capacitor_s21)

        def fit_to_model(capacitance_guess):
            # Define Parameters:
            params = Parameters()
            params.add("capacitance", value=capacitance_guess, vary=True)
            params.add("parasitic_inductance", value=1e-10, min=0, vary=True)
            params.add("length", value=4, vary=False)
            params.add("microstrip_width", value=microstrip_width, vary=False)
            params.add("dielectric_thickness", value=dielectric_thickness, vary=False)

            result = model.fit(
                network.s_db[:, 0, 1],
                params,
                data_frequency_array=network.f,
            )

            return result

        capacitance_guess = 1e-15
        result = fit_to_model(capacitance_guess=capacitance_guess)
        attempts = 1
        while np.mean(np.abs(result.residual)) > 1e-2:
            print(f"    Attempts: {attempts}")
            capacitance_guess += 1e-15
            result = fit_to_model(capacitance_guess=capacitance_guess)
            attempts += 1
            if attempts > 20:
                break

        if attempts > 20:
            print(f"No suitable solution for {file_path}")
            continue

        ideal = result.best_fit
        fit_capacitance = result.best_values["capacitance"]
        fit_parasitic_inductance = result.best_values["parasitic_inductance"]
        d = result.best_values["length"]

        fit_dictionary["dielectric_thickness"].append(dielectric_thickness)
        fit_dictionary["capacitor_width"].append(capacitor_width)
        fit_dictionary["capacitance"].append(fit_capacitance)
        fit_dictionary["parasitic_inductance"].append(fit_parasitic_inductance)

        # plt.figure(figsize=(8, 6))
        # plt.plot(network.f * 1e-9, network.s_db[:, 0, 1], linewidth=5, label="Sonnet")
        # plt.plot(
        #     network.f * 1e-9,
        #     ideal,
        #     color="k",
        #     linestyle="--",
        #     label=f"Skrf model with:\nC={fit_capacitance:.3E}F\nL={fit_parasitic_inductance:.3E}H",
        # )
        # plt.xlabel("Frequency (GHz)", fontsize=16)
        # plt.ylabel("S21 (dB)", fontsize=16)
        # plt.xticks(fontsize=14)
        # plt.yticks(fontsize=14)
        # plt.legend(fontsize=12)
        # plt.show()

        print(f"file {count} complete")

for key in fit_dictionary:
    fit_dictionary[key] = np.array(fit_dictionary[key])

np.savez(
    "../model_data/output_coupling_capacitor_data_arrays",
    cap_width_array=fit_dictionary["capacitor_width"],
    dielectric_thickness_array=fit_dictionary["dielectric_thickness"],
    capacitance_array=fit_dictionary["capacitance"],
    parasitic_inductance_array=fit_dictionary["parasitic_inductance"],
)
