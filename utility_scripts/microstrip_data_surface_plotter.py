import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate

data_file = np.load("../model_data/microstrip_data_arrays.npz")
microstrip_width_array = data_file["microstrip_width_array"]
dielectric_thickness_array = data_file["dielectric_thickness_array"]
mean_port_z0_array = data_file["mean_port_z0_array"]
mean_port_permittivity_array = data_file["mean_port_permittivity_array"]

x = dielectric_thickness_array * 1e9
y = microstrip_width_array * 1e6
z = np.real(mean_port_z0_array)

fig = plt.figure()

grid_x, grid_y = np.meshgrid(x, y)

ax = plt.axes(projection="3d")
ax.plot_trisurf(x, y, z, cmap="viridis", edgecolor="none")
ax.set_title("")
ax.set_xlabel("SiN thickness (nm)", fontsize=14)
ax.set_ylabel("Microstrip Width (um)", fontsize=14)
ax.set_zlabel("Mean Port $Z_0$ ($\Omega$)", fontsize=14)
ax.view_init(25, 140)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
ax.tick_params("z", labelsize=12)
plt.savefig("mean_port_Z0_surface.png", bbox_inches="tight", transparent="True")
plt.show()

x = dielectric_thickness_array * 1e9
y = microstrip_width_array * 1e6
z = np.real(mean_port_permittivity_array)

fig = plt.figure()

grid_x, grid_y = np.meshgrid(x, y)

ax = plt.axes(projection="3d")
ax.plot_trisurf(x, y, z, cmap="viridis", edgecolor="none")
ax.set_title("")
ax.set_xlabel("SiN thickness (nm)", fontsize=14)
ax.set_ylabel("Microstrip Width (um)", fontsize=14)
ax.set_zlabel("Mean Port $E_{r}$ ($\Omega$)", fontsize=14)
ax.view_init(25, 45)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
ax.tick_params("z", labelsize=12)
plt.savefig("mean_port_Er_surface.png", bbox_inches="tight", transparent="True")
plt.show()

microstrip_width = 2.0e-6
dielectric_thickness = 300e-9
z0_value = interpolate.griddata(
    (microstrip_width_array, dielectric_thickness_array),
    mean_port_z0_array,
    (microstrip_width, dielectric_thickness),
)

print(z0_value)
