import json

import matplotlib
import numpy as np
import click
import os
import matplotlib.pyplot as plt

from complete_tolerancing.model.FilterBankData import FilterBankData
import complete_tolerancing.complete_tolerancing_tools as complete_tolerancing_tools


def filter_bank_decoder(dictionary: dict):
    """
    Called for each dictionary in the JSON string to check if it can be converted to an object

    :param dictionary: The dictionary to be processed
    :return: The converted object or original dictionary
    """
    # Convert to complex numpy array
    if "__complex__" in dictionary:
        return np.array(dictionary["real"]) + 1j * np.array(dictionary["imag"])
    # Convert to numpy arrays
    if "__ndarray__" in dictionary:
        return np.array(dictionary["list"])
    return dictionary


def main():

    directory = r"simulated_data"
    resolution = 200

    save_figure = False

    lst = os.listdir(directory)  # your directory path
    number_of_files = len(lst)

    plt.figure(figsize=(10, 4))

    for count, filename in enumerate(os.listdir(directory)):
        file_path = os.path.join(directory, filename)

        if f"r{resolution}" not in filename:
            continue

        with open(file_path, "r") as file:
            filter_bank_json = file.read()

        spacing = float(filename[7:filename.find("-wave-spaced")])

        filter_bank_dict = json.loads(filter_bank_json, object_hook=filter_bank_decoder)
        filter_bank_data = FilterBankData(**filter_bank_dict)
        (
            total_power,
            channel_total_power,
        ) = complete_tolerancing_tools.get_total_power_arrays(
            filter_bank_s_params=filter_bank_data.ideal_filter_bank_s_params
        )

        plt.plot(
            filter_bank_data.frequency_array * 1e-9,
            channel_total_power,
            label=f"{spacing}$\lambda$",
            alpha=0.7
        )

    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("Total Power Transmission", fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=12)
    # plt.legend(fontsize=14, loc="center right", bbox_to_anchor=(1.35, 0.5))
    plt.legend(fontsize=12)
    plt.savefig(
        f"r{resolution}_fbs_channel_totals_at_various_filter_spacings.png",
        bbox_inches="tight",
    )
    plt.show()


if __name__ == "__main__":
    main()
