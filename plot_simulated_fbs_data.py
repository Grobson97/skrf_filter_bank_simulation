"""
Script to plot the S parameter data for a filter bank of a given oversampling.
The filter bank data is for the ideal case with no loss tangent, no etch error, and a
constant dielectric thickness of 0.5um and the realistic filter bank with the specified
loss tangent, random etch error per channel and a varying dielectric thickness.
"""

import json
import numpy as np
import click

from complete_tolerancing.model.FilterBankData import FilterBankData
import complete_tolerancing.complete_tolerancing_tools as complete_tolerancing_tools


@click.command()
@click.option(
    "--save-figure",
    default=False,
    help="Should the output graph be saved to a file?",
)
@click.option(
    "--file-name",
    type=click.STRING,
    required=True,
    help="JSON file containing filter bank simulation data",
)
def complete_tolerancing_plot_data(save_figure: bool, file_name: str):

    with open(file_name, "r") as file:
        filter_bank_json = file.read()

    def filter_bank_decoder(dictionary: dict):
        """
        Called for each dictionary in the JSON string to check if it can be converted to an object

        :param dictionary: The dictionary to be processed
        :return: The converted object or original dictionary
        """
        # Convert to complex numpy array
        if "__complex__" in dictionary:
            return np.array(dictionary["real"]) + 1j * np.array(dictionary["imag"])
        # Convert to numpy arrays
        if "__ndarray__" in dictionary:
            return np.array(dictionary["list"])
        return dictionary

    filter_bank_dict = json.loads(filter_bank_json, object_hook=filter_bank_decoder)
    filter_bank_data = FilterBankData(**filter_bank_dict)

    complete_tolerancing_tools.plot_filter_bank_data(
        frequency_array=filter_bank_data.frequency_array,
        realistic_filter_bank_s_params=filter_bank_data.realistic_filter_bank_s_params,
        ideal_filter_bank_s_params=filter_bank_data.ideal_filter_bank_s_params,
        frequency_min=120e9,
        frequency_max=180e9,
        save_figure=save_figure,
    )

    # frequency_min = filter_bank_data.frequency_min,
    # frequency_max = filter_bank_data.frequency_max,


if __name__ == "__main__":
    complete_tolerancing_plot_data()
