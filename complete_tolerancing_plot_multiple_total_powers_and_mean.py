"""
Script to plot the total channel throughput for multiple filterbank data files and compute the mean, min and max within
the filterbank band and plot this vs filter-bank wave spacing.

To use, put all data files to compare within a single directory and use the directory path in the script arguments.
"""
import json
import numpy as np
import click
import os
import matplotlib.pyplot as plt

from complete_tolerancing.model.FilterBankData import FilterBankData
import complete_tolerancing.complete_tolerancing_tools as complete_tolerancing_tools


@click.command()
@click.option(
    "--save-figure",
    default=False,
    help="Should the output graph be saved to a file?",
)
@click.option(
    "--directory",
    type=click.STRING,
    required=True,
    help="Directory containing JSON files containing filter bank simulation data",
)

def complete_tolerancing_plot_total_power_data(save_figure: bool, directory: str):
    def filter_bank_decoder(dictionary: dict):
        """
        Called for each dictionary in the JSON string to check if it can be converted to an object

        :param dictionary: The dictionary to be processed
        :return: The converted object or original dictionary
        """
        # Convert to complex numpy array
        if "__complex__" in dictionary:
            return np.array(dictionary["real"]) + 1j * np.array(dictionary["imag"])
        # Convert to numpy arrays
        if "__ndarray__" in dictionary:
            return np.array(dictionary["list"])
        return dictionary

    result_dict_1 = {
        "wave_spacing": [],
        "mean_total_power": [],
        "min_total_power": [],
        "max_total_power": [],
    }

    result_dict_2 = {
        "wave_spacing": [],
        "mean_total_power": [],
        "min_total_power": [],
        "max_total_power": [],
    }
    result_dict_3 = {
        "wave_spacing": [],
        "mean_total_power": [],
        "min_total_power": [],
        "max_total_power": [],
    }

    plt.figure(figsize=(8, 4))
    for count, filename in enumerate(os.listdir(directory)):
        file_path = os.path.join(directory, filename)

        with open(file_path, "r") as file:
            filter_bank_json = file.read()

        resolution = float(filename[1:4])
        spacing = float(filename[7:filename.find("-wave-spaced")])
        print(spacing)
        loss_tangent = float(filename[filename.find("loss-") + 5:filename.find("-data-points")])

        filter_bank_dict = json.loads(filter_bank_json, object_hook=filter_bank_decoder)
        filter_bank_data = FilterBankData(**filter_bank_dict)
        (
            total_power,
            channel_total_power,
        ) = complete_tolerancing_tools.get_total_power_arrays(
            filter_bank_s_params=filter_bank_data.realistic_filter_bank_s_params
        )

        line_style="-"
        if "r200" in filename:
            line_style = ":"
        if "r300" in filename:
            line_style = "--"

        plt.plot(
            filter_bank_data.frequency_array * 1e-9,
            channel_total_power,
            linestyle=line_style,
            label=f"R={resolution}, tand={loss_tangent}, spacing={spacing}",
        )

        band_min_index = np.where(filter_bank_data.frequency_array == 130e9)[0][0]
        band_max_index = np.where(filter_bank_data.frequency_array == 170e9)[0][0]
        if "r100" in filename:
            result_dict_1["wave_spacing"].append(spacing)
            result_dict_1["mean_total_power"].append(np.mean(channel_total_power[band_min_index: band_max_index]))
            result_dict_1["min_total_power"].append(np.min(channel_total_power[band_min_index: band_max_index]))
            result_dict_1["max_total_power"].append(np.max(channel_total_power[band_min_index: band_max_index]))
        if "r200" in filename:
            result_dict_2["wave_spacing"].append(spacing)
            result_dict_2["mean_total_power"].append(np.mean(channel_total_power[band_min_index: band_max_index]))
            result_dict_2["min_total_power"].append(np.min(channel_total_power[band_min_index: band_max_index]))
            result_dict_2["max_total_power"].append(np.max(channel_total_power[band_min_index: band_max_index]))
        if "r300" in filename:
            result_dict_3["wave_spacing"].append(spacing)
            result_dict_3["mean_total_power"].append(np.mean(channel_total_power[band_min_index: band_max_index]))
            result_dict_3["min_total_power"].append(np.min(channel_total_power[band_min_index: band_max_index]))
            result_dict_3["max_total_power"].append(np.max(channel_total_power[band_min_index: band_max_index]))

    # Convert to numpy arrays:
    for key in result_dict_1:
        result_dict_1[key] = np.array(result_dict_1[key])
    for key in result_dict_2:
        result_dict_2[key] = np.array(result_dict_2[key])
    for key in result_dict_3:
        result_dict_3[key] = np.array(result_dict_3[key])

    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("Total Power Transmission", fontsize=16)
    plt.xlim(120, 180)
    # plt.legend(fontsize=14, loc="center right", bbox_to_anchor=(1.35, 0.5))
    plt.legend(fontsize=14, loc='upper center', bbox_to_anchor=(0.5, -0.3), ncol=2)
    if save_figure:
        plt.savefig(
            f"filterbank_channel_totals_at_various_filter_spacings.png",
            bbox_inches="tight",
        )
    plt.show()

    plt.figure(figsize=(8, 6))
    # plt.fill_between(result_dict_1["wave_spacing"], result_dict_1["max_total_power"] * 100, result_dict_1["min_total_power"] * 100,
    #                  alpha=0.3, label="R=100 variation")
    # plt.fill_between(result_dict_2["wave_spacing"], result_dict_2["max_total_power"] * 100, result_dict_2["min_total_power"] * 100,
    #                  alpha=0.3, label="R=200 variation")
    # plt.fill_between(result_dict_3["wave_spacing"], result_dict_3["max_total_power"] * 100, result_dict_3["min_total_power"] * 100,
    #                  alpha=0.3, label="R=300 variation")
    plt.plot(result_dict_1["wave_spacing"], result_dict_1["mean_total_power"] * 100, label="R=100 Mean")
    plt.plot(result_dict_2["wave_spacing"], result_dict_2["mean_total_power"] * 100, label="R=200 Mean")
    plt.plot(result_dict_3["wave_spacing"], result_dict_3["mean_total_power"] * 100, label="R=300 Mean")
    plt.legend(fontsize=12)
    plt.xlabel("Filter Wave Spacing ($\lambda$)", fontsize=14)
    plt.ylabel("Total Filter-bank Throughput (%)", fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    if save_figure:
        plt.savefig(
            f"filterbank_mean_channel_totals_at_various_filter_spacings.png",
            bbox_inches="tight",
        )
    plt.show()

if __name__ == "__main__":
    complete_tolerancing_plot_total_power_data()
