"""
Script to plot the S parameter data for a filter bank of a given oversampling.
The filter bank data is for the ideal case with no loss tangent, no etch error, and a
constant dielectric thickness of 0.5um and the realistic filter bank with the specified
loss tangent, random etch error per channel and a varying dielectric thickness.
"""
import json
import numpy as np
import click
import os
import matplotlib.pyplot as plt

from complete_tolerancing.model.FilterBankData import FilterBankData
import complete_tolerancing.complete_tolerancing_tools as complete_tolerancing_tools


@click.command()
@click.option(
    "--save-figure",
    default=False,
    help="Should the output graph be saved to a file?",
)
@click.option(
    "--directory",
    type=click.STRING,
    required=True,
    help="Directory containing JSON files containing filter bank simulation data",
)
def complete_tolerancing_plot_total_power_data(save_figure: bool, directory: str):
    def filter_bank_decoder(dictionary: dict):
        """
        Called for each dictionary in the JSON string to check if it can be converted to an object

        :param dictionary: The dictionary to be processed
        :return: The converted object or original dictionary
        """
        # Convert to complex numpy array
        if "__complex__" in dictionary:
            return np.array(dictionary["real"]) + 1j * np.array(dictionary["imag"])
        # Convert to numpy arrays
        if "__ndarray__" in dictionary:
            return np.array(dictionary["list"])
        return dictionary

    plt.figure(figsize=(8, 4))
    line_colors = ["dimgrey", "k", "lightgrey"]
    line_styles = ["-", "-", "-"]

    for count, filename in enumerate(os.listdir(directory)):
        file_path = os.path.join(directory, filename)

        with open(file_path, "r") as file:
            filter_bank_json = file.read()

        loss_tangent = filename[
            filename.find("loss") + 5 : filename.find("-data-points")
        ]
        filter_bank_dict = json.loads(filter_bank_json, object_hook=filter_bank_decoder)
        filter_bank_data = FilterBankData(**filter_bank_dict)

        (
            total_power,
            channel_total_power,
        ) = complete_tolerancing_tools.get_total_power_arrays(
            filter_bank_s_params=filter_bank_data.realistic_filter_bank_s_params
        )

        if float(loss_tangent) == 0.0007:
            colour_map = plt.get_cmap("gist_rainbow_r")
            colors = [
                colour_map(i)
                for i in np.linspace(
                    0, 1, filter_bank_data.realistic_filter_bank_s_params.shape[0]
                )
            ]
            for index, s_param in enumerate(
                np.abs(filter_bank_data.realistic_filter_bank_s_params)
            ):
                if index == 0 or index == 1:
                    continue
                plt.plot(
                    filter_bank_data.frequency_array * 1e-9,
                    s_param**2,
                    color=colors[index],
                    linestyle=":",
                )
                line_colors[count] = "k"
                line_styles[count] = "-"

        plt.plot(
            filter_bank_data.frequency_array * 1e-9,
            channel_total_power,
            label="tan$\delta$ = %0.2e" % float(loss_tangent),
            linestyle=line_styles[count],
            color=line_colors[count],
        )

    plt.xlabel("Frequency (GHz)", fontsize=16)
    plt.ylabel("Power Transmission", fontsize=16)
    plt.xlim(115, 185)
    # plt.legend(fontsize=14, loc="center right", bbox_to_anchor=(1.35, 0.5))
    plt.legend(fontsize=14, loc="lower right")
    if save_figure:
        plt.savefig(
            f"filterbank_channel_totals_at_various_loss_tangents.png",
            bbox_inches="tight",
        )
    plt.show()


if __name__ == "__main__":
    complete_tolerancing_plot_total_power_data()
