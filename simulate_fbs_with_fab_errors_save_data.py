"""
Script to generate simulation S parameter data for a filter bank of a given oversampling.
The filter bank data is for the ideal case with no loss tangent, no etch error, and a
constant dielectric thickness of 0.5um and the realistic filter bank with the specified
loss tangent, random etch error per channel and a varying dielectric thickness.
"""

import dataclasses
import json

import click
import numpy as np

import complete_tolerancing.complete_tolerancing_tools as complete_tolerancing_tools


@click.command()
@click.option(
    "--resolution",
    type=click.FLOAT,
    required=True,
    help="Filter-bank resolution (300 or 200)",
)
@click.option(
    "--oversampling",
    type=click.FLOAT,
    required=True,
    help="Channel oversampling amount",
)
@click.option(
    "--loss-tangent",
    type=click.FLOAT,
    required=True,
    help="Dielectric loss tangent",
)
@click.option(
    "--filter-spacing",
    type=click.FLOAT,
    required=True,
    help="Filter spacing relative to wavelength",
)
@click.option(
    "--frequency-min",
    type=click.INT,
    required=True,
    help="The frequency band minimum",
)
@click.option(
    "--frequency-max",
    type=click.INT,
    required=True,
    help="The frequency band maximum",
)
@click.option(
    "--start",
    type=click.INT,
    required=True,
    help="The start",
)
@click.option(
    "--stop",
    type=click.INT,
    required=True,
    help="The stop",
)
@click.option(
    "--number-of-points",
    type=click.INT,
    required=True,
    help="The number of points",
)
def complete_tolerancing_save_data(
    resolution: int,
    oversampling: float,
    loss_tangent: float,
    filter_spacing: float,
    frequency_min: int,
    frequency_max: int,
    start: int,
    stop: int,
    number_of_points: int,
):
    filter_bank_data = complete_tolerancing_tools.get_filter_bank_data(
        oversampling=oversampling,
        resolution=resolution,
        loss_tangent=loss_tangent,
        filter_spacing=filter_spacing,
        frequency_min=frequency_min,
        frequency_max=frequency_max,
        start=start,
        stop=stop,
        number_of_points=number_of_points,
    )

    class NumpyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, np.ndarray):
                if obj.dtype == np.complex128:
                    return {
                        "__complex__": True,
                        "real": obj.real.tolist(),
                        "imag": obj.imag.tolist(),
                    }
                return {"__ndarray__": True, "list": obj.tolist()}
            return super().default(obj)

    filter_bank_dict = dataclasses.asdict(filter_bank_data)
    filter_bank_json = json.dumps(filter_bank_dict, cls=NumpyEncoder, indent=2)

    file_name = (
        f"r{resolution}-"
        f"{filter_spacing}-wave-spaced"
        f"-{filter_bank_data.number_of_channels}-channel"
        f"-{frequency_min}-{frequency_max}-GHz"
        f"-filter-bank-simulation-data-with"
        f"-oversampling-{oversampling}"
        f"-loss-{loss_tangent}"
        f"-data-points-{number_of_points}.json"
    )
    with open(file_name, "w") as file:
        file.write(filter_bank_json)

    click.echo(f"Data saved to {file_name}.")


if __name__ == "__main__":
    complete_tolerancing_save_data()
