py # Scikit Filter Bank Simulation
Simulating the RF performance of an on-chip superconducting filterbank with half-wave resonator filters using the Scikit-RF module.

## To use

### To simulate and save the data to a JSON file:
#### For full band simulation:
```bash
py simulate_fbs_with_fab_errors_save_data.py --resolution 200 --oversampling 1.6 --loss-tangent 7e-4 --filter-spacing 0.25 --frequency-min 120 --frequency-max 180 --start 100 --stop 200 --number-of-points 1001
```
#### For a quick test simulation:
```bash
py simulate_fbs_with_fab_errors_save_data.py --resolution 200 --oversampling 1.6 --loss-tangent 7e-4 --filter-spacing 0.25 --frequency-min 149 --frequency-max 151 --start 140 --stop 160 --number-of-points 101
```

### To plot data from JSON file:
```bash
py plot_simulated_fbs_data.py --save-figure False --file-name my-file.json
```

## Setup

Create a virtual environment:
```bash
python -m venv venv
```

activate environment:
```bash
.\venv\Scripts\activate
```

To test the software runs correctly run a test simulation:
```bash
py complete_tolerancing_save_data.py --resolution 200 --oversampling 1.6 --loss-tangent 7e-4 --filter-spacing 0.25 --frequency-min 149 --frequency-max 151 --start 140 --stop 160 --number-of-points 101
```

Then plot the data and compare with the plot in set_up_test:
```bash
py complete_tolerancing_plot_data.py --save-figure True --file-name r200.0-0.25-wave-spaced-4-channel-149-151-GHz-filter-bank-simulation-data-with-oversampling-1.6-loss-0.0007-data-points-101.json
```


NB: Remember to comment out the correct code for microstrip arrays depending on R=200 or R=300 in scikit_filter_bank_simulation/util/filter_bank_model_tools.py get_media_properties
