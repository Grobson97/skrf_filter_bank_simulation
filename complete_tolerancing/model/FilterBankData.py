from dataclasses import dataclass

import numpy as np


@dataclass
class FilterBankData:
    frequency_array: np.ndarray
    realistic_filter_bank_s_params: np.ndarray
    ideal_filter_bank_s_params: np.ndarray
    frequency_min: float
    frequency_max: float
    number_of_channels: int

    @classmethod
    def init_from_dict(cls, filter_bank_data_dict: dict):
        filter_bank_data = FilterBankData()
        for key, value in filter_bank_data_dict.items():
            setattr(filter_bank_data, key, value)
